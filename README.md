# Mémo JavaScript - Enseignements à l'Esad-Valence option Design Graphique

Mémo JS est un ensemble de ressources concernant la programmation avec le langage JavaScript, dans le contexte des technologies du Web.

Il s'agit d'une forme écrite et consultable de certains cours proposés par Dominique Cunin dans le cadre de ses enseignements à [l'École Supérieure d'Art de Valence](http://esad-gv.fr).

Cette initiative fait suite à une requête des étudiants, qui désiraient pouvoir accéder à des ressources en dehors des cours et des entretiens individuels, sans qu'il s'agisse pour autant de cours en ligne tiers.

Un investissement plus important dans ce format a été fourni à l'occasion de la période de confinement imposée à la France et à de nombreux pays du monde pendant la pandémie du Coronavirus de 2019-2020.

## Usage

Cloner le répertoire sur son disque local, puis :

> npm install


## Structure de l'outil et fonctionnalités

Le langage [MarkDown](https://www.markdownguide.org/basic-syntax/) est utilisé pour créer les contenus à afficher. Il faut donc respecter la syntaxe de ce langage pour que les documents soient bien interprétés.

Quelques fonctionnalités "maison" ont été ajoutées spécifiquement pour cet outil. Il s'agit parfois d'extensions au MarkDown déjà disponibles à travers la librairie de conversion de MarkDown en HTML qui est ici utilisée ([markdown-it](https://github.com/markdown-it/markdown-it) et [markdown-it-attrs.browser](https://github.com/arve0/markdown-it-attrs)).

- Possibilité de définir une destination pour les liens :
```
[mon lien](url_du_lien){target=blank}
```

Les valeurs habituelles de l'attribut ```target``` des éléments ```<a>``` peuvent être utilisés.

- Possibilité de définir un style particulier pour un paragraphe :
```
Ma phrase.{style="opacity:0"}
```
Ici, le paragrpahe "Ma phrase." sera totalement transparent.

- Possibilité d'insérer une iFrame exécutant le code source présenté :

    Un parser complémentaire a été ajouté à ```highlight.js```, librairie qui permet de générer un affichage stylisé des codes sources en HTML en associant automatique une feuille de style de colorisation des mots clés, langage par langage.
    En ajoutant le préfix ```render-<lettre>``` au code source que vous voulez faire afficher dans une iFrame d'exécution, différentes sources peuvent être associées entre elles.

```

```

## Comment ajouter un épisode

Il suffit d'ajouter un fichier ```.md``` dans le dossier ```md``` et d'y écrire l'épisode en question en respectant le format Markdown.

Dans le fichier index.md, il faut ajouter un lien dans la liste des liens :

```
[Convertir un texte en span](?mdpath=md/text2span.md)
```

L'URL du lien doit impérativement être relative et transmettre un paramètre ```?mdpath=md/pathToYourFile.md``` où pathToYourFile doit être remplacé par le nom de votre nouveau fichier.