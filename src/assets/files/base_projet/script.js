/*
Au click dans la fenêtre, des div apparaissent dans la page à des coordonnées aléatoires.
*/

//ajout de l'écouteur d'événement à la fenêtre
window.addEventListener("click", addNewDiv);

//callback d'ajout d'une div
function addNewDiv() {
    var div = document.createElement("div");
    div.style.position = "absolute";
    div.style.border = "solid 1px";
    div.style.width = "100px";
    div.style.height = "100px";
    document.body.appendChild(div);

    var x = randomFromTo(0, window.innerWidth);
    var y = randomFromTo(0, window.innerHeight);

    div.style.top = y + "px";
    div.style.left = x + "px";
}

function randomFromTo(from, to) {
    var result = Math.random() * (to - from) + from;
    return result; // valeur qui est retourné à l'appel de fonction
}