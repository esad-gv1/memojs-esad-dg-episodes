[Retour à l'index](index.html)
# Mémo JavaScript

## Contre l'effet page blanche!

### Les trois fichiers

Une structure de départ : un fichier index.html (les serveurs web vont chercher automatique la page nommée index.html), ce fichier permet l'accès aux deux autres, la feuille de style (style.css, index.css, ou ce que vous voulez tant que ça a du sens pour vous!), et le fichier JavaScript (index.js ou autre...).

### L'écriture du programme

#### Pour commencer

Un programme représente des actions qui seront exécutées à postériori de leur écriture. Il est donc nécessaire de construire une image mentale du programme que l'on souhaite écrire avant de se lancer dans l'écriture du code en lui-même.

Trois grandes phases de description de votre projet à insérer dans les commentaires du code source :

1. Description intuitive, comme si on décrivait le projet à un ami, ou en tout cas à un humain. Écrire peu de phrases, ne pas rentrer dans les détails, aller l'essentiel.

2. Décrire les blocs fonctionnels, entrer dans les détails des actions, **spécifier le projet** d'un point de vue technique, mais rester dans un dialogue avec un humain à qui vous souhaitez transmettre les éléments techniques dont votre projet à besoin. Décrire de manière précise ce que doit faire le projet. Il s'agit de lever autant que possible les **non-dits**.

3. Entrer dans les détails techniques et commencer à trouver les solutions d'implémentation. Il s'agit de chercher les éléments dans l'environnement technique avec lequel on travaille (ici, le Web). Comprendre les ensembles fonctionnels (interactivité, automatisation, etc.), préparer une structure globale pour le programme et trouver des références techniques (tutos, exemples, documentations, etc.).

4. Convertir les descriptions faites aux étapes précédentes en programme : il faut donc connaître un minimum de technique.


#### Rappels : éléments techniques de programmation

JavaScript et CSS permettent beaucoup de choses ensemble. Néanmoins, seul JS permet d'écrire véritablement l'interactivité.

Un exemple basique.

1. Une div présente dans la page permet, au click, de générer d'autres div dans la page à des positions random.

2. La div qui sert de bouton a une couleur de fond, mais pas les div générées.

3. Les éléments techniques à utiliser sont les suivants :

* Variables
        
    Permettent de stocker des données quelconques pour les utiliser au cours du programme. Ces données peuvent varier au cours du temps et permettre de faire des calculs ou d'autres manipulations d'informations. Elles sont déclarées avec le mot-clé `var`.
        
```javascript
// On déclare la variable nommée maVariable à laquelle on affecte, avec l'opérateur d'affectation, la valeur 10
var maVariable = 10;
```
* Événements

    Les événements sont attachés à des objets présents dans la page (div, paragraphe, etc.) et permettent d'exécuter un bloc de code lorsque l'événement est appelé.

[Références sur les événements en JS DOM](https://developer.mozilla.org/fr/docs/Web/Events){target=blank}

```javascript
//on ajoute ainsi un écouteur d'événements à un élément HTML. Le 1er argument doit être trouvé dans la liste des événements possibles (la documentation vous renseigne à ce sujet), le 2e argument est une fonction qui contient le bloc d'instructions qui seront exécutées lors de l'appel d'événement

element.addEventListener("click", callback);

//la fonction de retour (callback) contient le bloc de code qui sera exécuté lors de l'événement

function callback()
{
  //implémentation à écrire
}
```
* Fonctions

    Les fonctions permettent la réutilisation de code source pour résoudre des cas génériques. Elles prennent des arguments et peuvent fournir des valeurs de retour (à l'aide du mot-clé `return`). Elles sont déclarées avec le mot-clé `function`.

```javascript
//on déclare la fonction randomFromTo pour qu'elle prenne 2 arguments qui seront utilisés pour effectuer le calcul de tirage aléatoire.
function randomFromTo(from, to)
{
    var result = Math.random() * (to - from) + from;
    return result; //on retourne la valeur de la variable result
}
```

##### Problèmes à résoudre :

* Création de nouvelles div en JS -> DOM, car s'il s'agit d'un élément HTML, alors il faut manipuler l'arbre HTML du document. Les fonctions intégrées dans le DOM contiennent peut-être déjà une solution.

[Référence de createElement](https://developer.mozilla.org/fr/docs/Web/API/Document/createElement){target=blank}


```javascript
//une variable nous permettra d'avoir une référence pour la suite du programme

var newElement = document.createElement("type");
document.body.appendChild(newElement);
```

* Comment positionner la div nouvellement crée dans la page : passer par son style via JS. Attention!! Les styles attendent comme valeur une chaîne de caractères qui contient la valeur numérique et l'unité à appliquer (comment défini dans CSS). Il faut donc se renseigner sur les champs CSS correspondant pour manipuler les styles des éléments via JS.

```javascript
element.style.top = valeur + "px";
element.style.left = valeur + "px";
```

Il faut récupérer des valeurs aléatoires à l'aide de notre fonction de random. Comment s'y prendre ?

```javascript
//pour appeler la fonction avec ses 2 arguments (from et to) :
randomFromTo(0, window.innerWidth);

//pour stocker la valeur de retour de la fonction et l'utiliser plus tard :
var x = randomFromTo(0, window.innerWidth);
```

Notre programme devient donc :

```javascript
render-a/*
Une div permet, au click, de produire d'autres div dans la page.
*/

//Une référence à l'élément concerné
var div1 = document.getElementById("div1");

//ajout de l'écouteur d'événement
div1.addEventListener("click", addNewDiv);

function addNewDiv()
{
    var div = document.createElement("div");
    document.body.appendChild(div);
    
    var x = randomFromTo(0, window.innerWidth);
    var y = randomFromTo(0, window.innerHeight);

    div.style.top =  y + "px";
    div.style.left = x + "px";
}

function  randomFromTo(from, to)
{
    var result = Math.random() * (to - from) + from;
    return result; // valeur qui est retourné à l'appel de fonction
}
```
```html
render-a<html>

  <head>
    <meta charset="utf8">
    <link type="text/css" href="style.css" rel="stylesheet">
  </head>

  <body>
    <div id="div1"></div>
    <script src="index.js"></script>
  </body>
  
</html>
```

```css
render-ahtml, body {
    margin: 0px;
}

div {
    position: absolute;
    border: solid 1px;
    width: 100px;
    height: 100px;
}
```
[Retour à l'index](index.html)

Fin.{style="opacity:0"}