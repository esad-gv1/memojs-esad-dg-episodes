[Retour à l'index](index.html)
# Mémo JavaScript

## Image-Map : image à zones sensibles, version complexe!


```css
render-chtml,
body {
  margin: 0px;
  width: 100%;
  height: 100%;
}

#map {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
}
```

Il s'agit ici d'une astuce (voire d'un *hack*) : en forçant une situation a priori impossible avec top, bottom, left et right à 0, la gestion automatique des marges place l'image au centre de la page!

## Code source complet

```html
render-a<html>

    <head>
        <meta charset="utf8">
        <link type="text/css" href="style.css" rel="stylesheet">
    </head>

    <body>
        <div id="container">
            <img src="rsrc/img/grid.png" id="map">
            <div id="zone1" class="zone"></div>
            <div id="zone2" class="zone"></div>
        </div>

        <div id="infoZone1">
            Ce sont des informations sur la zone 1
        </div>

        <script src="script.js"></script>
    </body>

</html>
```

```javascript
render-awindow.addEventListener("resize", windowResizeHandler);

var map = document.getElementById("map");
var zone1 = document.getElementById("zone1");
var zone2 = document.getElementById("zone2");

var zones = [];
zones.push(zone1);
zones.push(zone2);

var zoneStyles = [];

for (var i = 0; i < zones.length; i++) {

    var zone = zones[i];

    var object = {};

    object.element = zone;

    var styles = {
        "width": getComputedStyle(zone).width,
        "height": getComputedStyle(zone).height,
        "left": getComputedStyle(zone).left,
        "top": getComputedStyle(zone).top,
    };

    for (key in styles) {
        if (key != "element") {
            styles[key] = styles[key].replace(/px/, "");
            styles[key] = Number(styles[key]);
        }
    }
    object.styles = styles;

    zoneStyles[i] = object;
}

var infoZone1 = document.getElementById("infoZone1");

zone1.addEventListener("mouseenter", mouseEnterHandler);
zone1.addEventListener("mouseleave", mouseLeaveHandler);

function mouseEnterHandler() {
    console.log("enter!!!")
    infoZone1.style.display = "block";
}

function mouseLeaveHandler() {
    console.log("leave!!!");
    infoZone1.style.display = "none";
}

windowResizeHandler();

function windowResizeHandler() {

    for (var i = 0; i < zoneStyles.length; i++) {

        var zone = zoneStyles[i].element;
        var style = zoneStyles[i].styles;

        var zoneX = style.left;
        var zoneY = style.top;

        var newX = map.offsetLeft + (zoneX / (map.naturalWidth / map.offsetWidth));
        var newY = map.offsetTop + (zoneY / (map.naturalHeight / map.offsetHeight));

        var newW = style.width / (map.naturalWidth / map.offsetWidth);
        var newH = style.height / (map.naturalHeight / map.offsetHeight);

        zone.style.left = newX + "px";
        zone.style.top = newY + "px";
        zone.style.width = newW + "px";
        zone.style.height = newH + "px";
    }
}
```

```css
render-ahtml,
body {
  margin: 0px;
  width: 100%;
  height: 100%;
}

#container {
  position: absolute;
  width: 80%;
  height: 80%;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
}

#map {
  position: absolute;
  max-width: 100%;
  max-height: 100%;
  margin: auto;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
}

.zone {
  position: absolute;
}

#zone1 {
  left: 43px;
  top: 42px;
  width: 78px;
  height: 78px;
  background-color: blue;
}

#zone2 {
  left: 865px;
  top: 865px;
  width: 150px;
  height: 150px;
  background-color: blue;
}

#infoZone1 {
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100px;
  height: 200px;
  background-color: brown;
  display: none;
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}