[Retour à l'index](index.html)

# Mémo JavaScript

## Structure de controle : la répétition avec _for_

Il est fréquent de devoir répeter certaines instructions dans des programmes. Les cas d'usage sont si nombreux qu'on peut dire qu'il y en a une infinité : agir sur plusieurs formes graphique en même temps, créer plusieurs objets d'une seule action, parcourir des ensembles de données pour les analyser et en faire une représentation, etc.

Il existe plusieurs structure de contrôle pour construire des boucle de répitition. ```while``` en fait partie, mais c'est ```for``` que l'on retrouve le plus souvent.

## Créer plusieurs objets d'un coup, d'un seul!

Nous savons comment générer un élément HTML via JavaScript. Mais comment en générer une multitude ?

```javascript
var div = document.createElement("div");
document.body.appendChild(div);

var div2 = document.createElement("div");
document.body.appendChild(div2);

var div3 = document.createElement("div");
document.body.appendChild(div3);
```

Le principe est là, mais il y a ici comme quelque chose de répétitif... d'ennuyant même, dans ce code source. Pourquoi devrions-nous réécrire autant de fois les mêmes lignes de code ? **L'ordinateur est pourtant bien une machine ayant la capacité d'automatiser des tâches relevant du calcul et de la logique!** Une solution plus efficace, plus économe en nombre de lignes de code, doit exister :

```javascript
for(var index = 0; index < 3; index = index + 1 ) {
    console.log(index);
    var div = document.createElement("div");
    document.body.appendChild(div);
}
```
résultat dans la console :
```
0
1
2
```
Une boucle ```for```, démontrée ci-dessus, permet de répéter un certain nombre de fois le bloc d'instructions qu'il définit.

## Syntaxe 

La structure globale d'une boucle for est celle-ci :

```javascript
for ( ; ; ) {
    //bloc d'instructions à exécuter
}
```

Les deux point-virgules nous montrent que les trois arguments attendus dans les parenthèses sont en réalité **trois instructions** :

1. Initialisation (déclaration et affectation) d'une variable servant de "compteur".
2. Expression de la condition évaluée à chaque itération de la boucle.
3. Modification de la variable "compteur" à chaque itération.

```javascript
for( initialisation; condition ; modification) {
    //expressions à exécuter en boucle
}
```

## Dans les détails

Prenons un exemple basique pour en analyser les étapes.

```javascript
for( var index = 0; index < 5 ; index = index + 1) {
    //expressions à exécuter en boucle
    //...
    console.log("index = " + index);
}
console.log("fin de la boucle!");
```

Lorsque l'interpréteur JavaScript exécute cette boucle, voici comment il gère les choses, étape par étape :

1. La variable ```index``` est déclarée en mémoire, la valeur ```0``` lui est affectée à l'aide de l'opérateur d'affectation ```=```.
2. La condition ```index < 5``` est évaluée : ```index``` vaut ```0```, ce qui est inférieur à ```5```, la condition est donc remplie (```true```).
3. Le bloc d'instructions est exécuté, ici une simple expression : ```console.log("index = " + index);```. La console imprime donc ceci ```"index = 0"```.
4. La modification est exécutée : ```index = index + 1```, donc index, qui vaut 0, se voit ajouter 1, et prend la valeur ```1```.

Cette première exécution de l'ensemble de l'instruction ```for``` est donc la première **itération** de la boucle.

L'étape 1, déclaration de la variable compteur, est exécutée **uniquement** lors de cette première itération. Elle ne le sera plus par la suite.
Cette séquence sera répétée (itérée) jusqu'à ce que **la condition ```index < 5``` ne soit plus remplie**, c'est-à-dire à l'itération 5 de la boucle, lorsque ```index``` vaut ```5``` :

1. La condition ```index < 5``` est évaluée : ```index``` vaut ```5```, ce qui n'est pas inférieur à ```5```, la condition n'est donc pas remplie.
2. On sort de la boucle, l'instruction suivante est donc atteinte, la console affiche : ```"fin de la boucle!"```.

Le résultat complet, dans la console, de l'exécution du programme est alors celui-ci :

```
> index = 0
> index = 1
> index = 2
> index = 3
> index = 4
> fin de la boucle!
```
Une pratique très courante consiste à utiliser des noms de variables très courts, ```i```, remplaçant ```index```, et la forme ```i++```, contractation de ```index = index + 1``` étant favorisé : 

```javascript
for(var i = 0; i < 3; i++ ) {
    console.log(i);
    var div = document.createElement("div");
    div.style.width = "100px";
    div.style.height = "100px";
    div.style.border = "solid 1px black";
    document.body.appendChild(div);
}
```
## Exemple complet

Au click de la souris, plusieurs éléments, dont le nombre est donnée par ```elementTotal``` sont créés dans la page. On inscrit dans les div le nombre qui est utilisé dans la boucle ```for``` pour représenter l'éxécution du processus.

```javascript
render-awindow.addEventListener("click", onClick);

var elementTotal = 5;

function onClick(event) {
  for (var i = 0; i < elementTotal; i++) {
    var newDiv = document.createElement("div");
    newDiv.style.width = "50px";
    newDiv.style.height = "50px";
    newDiv.style.border = "1px solid black";
    newDiv.innerHTML = i;
    document.body.appendChild(newDiv);
  }
}
```
```css
render-abody {
  display: inline-flex;
  flex-wrap: wrap;
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}
