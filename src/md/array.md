[Retour à l'index](index.html)

# Mémo JavaScript

## Les tableaux : Array

Un [Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array){target=blank} est un objet complexe présent dans de nombreux langages de programmation. On peut aussi les appeler des **listes**, car il s'agit de pouvoir stocker, dans une seule et même variable, une liste de valeurs.

## Des meubles à tiroirs simples ou multiples...

Utilisons une métaphore pour comprendre la différence entre une variable ne contenant qu'une valeur, quel que soit son type, et une variable contenant un tableau.

![](assets/img/variable.svg){style="width: 30%; height: auto; margin-left: 35%;"}

⬆︎ Une variable "simple" est comme un meuble de rangement qui ne possède qu'un seul tiroir. Le nom du meuble (Ma_Variable) permet d'aller trouver ce meuble en particulier dans l'ensemble de tous les meubles disponibles dans la mémoire de l'ordinateur.

Ainsi, lorsqu'on appelle "Ma_Variable" dans notre programme, le meuble est identifié, puis son tiroir est ouvert pour récupérer la valeur qu'il contient.

⬇︎ Un tableau est aussi un meuble de stockage en mémoire, mais il contient plusieurs tiroirs. Pour le retrouver parmi les autres, il faut donc lui donner un nom (Mon_Tableau), comme pour une variable "simple".

![](assets/img/tableau.svg){style="width: 30%; height: auto; margin-left: 35%;s"}

Mais ce seul nom, s'il permet d'identifier le meuble en lui-même, ne permet pas de récupérer une valeur particulière, car plusieurs tiroirs peuvent être ouverts. Il est donc indispensable de pouvoir identifier le tiroir que l'on veut ouvrir pour en récupérer la valeur!

Comme le montre le schéma, ci-dessus, une convention a été décidée pour permettre l'accès individuel à ces tiroirs : chacun possède un index, commençant à partir de 0, puis augmente d'une unité, tiroir par tiroir. Il peut y avoir autant de tiroirs que l'on souhaite (dans la limite de la mémoire disponible...). Dans le meuble dessiné ci-dessus, nous voyons qu'il y a 4 tiroirs, avec des index allant de 0 à 3.

Pour accéder au 2e tiroir, c'est-à-dire à l'index 1, il faudrait donc pouvoir écrire quelque chose comme : `Mon_Tableau.tirroir-1`.
La syntaxe la plus fréquente pour écrire ceci utilise les crochets (**brackets**) `[]` (à ne surtout pas confondre avec les accolades (_curly brackets_) `{}` utilisées pour délimiter les blocs d'instructions).

Voici comment déclarer et manipuler un tableau en JavaScript.

```javascript
//déclaration d'un tableau vide
var tableau = [];

//affectation d'une valeur à l'index 0
tableau[0] = 35;

//affectation d'une valeur à l'index 1
tableau[1] = 56;

//vérification/récupération du contenu de l'index 1
console.log(tableau[1]);
// > 56

//déclaration d'un tableau contenant des nombres
var tableau2 = [35, 56, 20, 80];
//vérification du contenu du tableau
console.log(tableau2);
// > [35, 56, 20, 80]

//déclaration d'un tableau contenant diverses données
var tableau3 = ["un", 48, "un mot", 43.333];

//récupération du nombre d'éléments dans le tableau
console.log(tableau3.length);
// > 4
```

Un tableau peut donc contenir un nombre arbitraire d'éléments de tous types confondus (donc, il peut aussi contenir des éléments HTML).

## Automatisation et parcours des Array

### Construction d'un Array

Si un tableau contient de très nombreux éléments, il devient vite difficile de les manipuler avec des instructions permettant l'accès à un seul élément à la fois. Il est donc particulièrement pertinent d'utiliser des boucles de répétition lorsqu'on manipule des tableaux.

```javascript
//déclaration d'un tableau vide
var tableau = [];

//nombre d'éléments qu'on veut crée dans le tableau
var maxElement = 10;

//remplissage à l'aide d'une boucle for
for (var i = 0; i < maxElement; i++) {
  tableau[i] = "element" + i;
}

console.log(tableau);
// > (10) ["element0", "element1", "element2", "element3", "element4", "element5", "element6", "element7", "element8", "element9"]
```

**Mais... qu'est-ce qu'il se passe ici...??**

Le secret est dans la boucle `for`, qui n'a, en réalité, rien de compliqué.

D'après ce que l'on a déjà pu voir dans [l'épisode consacré aux boucles](?mdpath=md/for.md) `for`, on comprend qu'une variable locale à la boucle est utilisée (`i`), que son état initial a pour valeur `0` et qu'elle sera incrémentée d’une unité à chaque itération (`i++`) tant qu'elle n'aura pas atteint la valeur contenue dans la variable `maxElement`, c'est à dire, ici, `10`.

On obtient donc pour `i` une valeur qui vaut `0`, puis `1`, puis `2`, etc. On peut donc utiliser cette variable pour accéder à des élements du tableau, et même pour créer le contenu de chaque cellule.

```javascript
for (var i = 0; i < maxElement; i++) {
  tableau[i] = "element" + i;
}
```

Lorsque `i` vaut `0`, on obtient `tableau[0] = "element" + 0;`, qui va affecter la valeur `"element" + 0`, c'est-à-dire `"element0"` à l'index `0` du tableau, ce qui produit :

```
tableau = ["element0"]
```

Lorsque `i` vaut `1`, on obtient `tableau[1] = "element" + 1;`, qui va affecter la valeur `"element" + 1`, c'est-à-dire `"element1"` à l'index `1` du tableau, ce qui produit :

```
tableau = ["element0", "element1"]
```

...Et ainsi de suite jusqu'à la dernière valeur possible pour `i`, qui est affectée avec `var maxElement = 10`.

### Parcours d'un Array existant

Lorsqu'on obtient un tableau comme réponse à un appel de fonction, il faut savoir le manipuler pour en extraire les informations pertinentes pour le projet. De nombreux cas sont possibles, mais voyons-en deux principaux : le parcours d'un tableau qu'on a créé nous-mêmes, et le parcours d'un tableau d'élément HTML provenant d'un document existant.

#### Array.length

Si on poursuit notre exemple minimal précédent, nous pouvons tenter de modifier le contenu des éléments du tableau après leur création. Admettons que l'on souhaite transformer le contenu des cellules en une chaîne disant `"index de cellule ="` suivi du n° de la cellule.

```javascript
// la variable tableau existe déjà, elle provient du fragement de code ci-dessus.

for (var i = 0; i < tableau.length; i++) {
  tableau[i] = "index de cellule = " + i;
}
console.log(tableau);
// > (10) ["index de cellule = 0", "index de cellule = 1", "index de cellule = 2", "index de cellule = 3", "index de cellule = 4", "index de cellule = 5", "index de cellule = 6", "index de cellule = 7", "index de cellule = 8", "index de cellule = 9"]
```

La propriété **length** d'un tableau retourne sa longueur, c'est-à-dire le nombre d'éléments qu'il contient. Cette propriété est particulièrement pratique, car elle permet au code source de s'adapter automatiquement à la longueur du tableau que l'on souhaite traiter.

Le tableau ci-dessous représente quelques exemples de contenus d'Array et leur valeur `length` :

| array                 | array.length |
| --------------------- | ------------ |
| `[0]`                 | 1            |
| `[0,1,2,3,4]`         | 5            |
| `[0,1,2,3,4,6,7,8,9]` | 10           |

On voit donc que l'index 0 compte comme étant un élément, ce qui peut porter à confusion.

#### document.getElementsByTagName

Dans un document HTML, on peut avoir créé une grande quantité d'élements sur lesquels on souhaite effectuer un traitement après leur création. Par exemple, cherchons tous les hyperliens dans une petite page HTML (un fragment de Wikipédia) pour injecter une action particulière au click à l'aide de JavaScript.

```html
render-a<html>

    <head>
        <meta charset="utf8" />
        <link type="text/css" href="style.css" rel="stylesheet" />
    </head>

    <body>
        <p>
            La <b>cybernétique</b> est l'étude des mécanismes d'<a href="/wiki/Information" title="Information">information</a>
            des <a href="/wiki/Syst%C3%A8me" title="Système">systèmes</a> complexes,
            explorés en vue d'être standardisés lors des
            <a href="/wiki/Conf%C3%A9rences_Macy" title="Conférences Macy">conférences Macy</a>
            et décrits en 1947 par
            <a href="/wiki/Norbert_Wiener" title="Norbert Wiener">Norbert Wiener</a>
            dans ce but. Des scientifiques d'horizons très divers et parmi les plus
            brillants de l'époque participèrent à ce projet interdisciplinaire de 1942
            à 1953&nbsp;: mathématiciens, logiciens, ingénieurs, physiologistes,
            anthropologues, psychologues, etc. Les contours parfois flous de cet
            ensemble de recherches s'articulent toutefois autour du concept clé de
            <i><a href="/wiki/R%C3%A9troaction" title="Rétroaction">rétroaction</a></i>
            (en anglais <i><span class="lang-en" lang="en">feedback</span></i>) ou
            <i><a href="/wiki/T%C3%A9l%C3%A9ologie" title="Téléologie">mécanisme téléologique</a></i>. Leur but était de donner une vision unifiée des domaines naissants de
            l'<a href="/wiki/Automatique" title="Automatique">automatique</a>, de l'<a href="/wiki/%C3%89lectronique_(technique)" title="Électronique (technique)">électronique</a>
            et de la
            <a href="/wiki/Th%C3%A9orie_de_l%27information" title="Théorie de l'information">théorie mathématique de l'information</a>, en tant que «&nbsp;théorie entière de la commande et de la
            communication, aussi bien chez l'animal que dans la machine&nbsp;».
        </p>
    </body>

</html>
```

On voit ici que la structure HTML, parce qu'elle est générée par un logiciel d'édition spécifique, est peu lisible dans le code source qu'elle produit. Si ce copier-coller permet d'afficher tout de même le texte (sans son style d'origine), on voit que les liens sont ceux de la page d'origine, ils pointent donc vers d'autres pages de Wikipédia.

Essayons de bloquer la navigation, de récupérer tous les liens présents dans ce fragment pour afficher, au click sur l'hyperlien, une alerte montrant l'URL vers laquelle nous devrions aller.

Pour récupérer l'ensemble des occurrences d'un élément HTML dans une page, on peut utiliser la méthode [getElementsByTagName](https://developer.mozilla.org/fr/docs/Web/API/Element/getElementsByTagName), qui prend un argument, une chaîne de caractère représentant l'élément à récupérer. Cette méthode renvoie un Array!

Pour obtenir tous les éléments ```div``` d'une page HTML, on utilisera : ```document.getElementsByTagName("div");```. Ce qui nous donnera un tableau d'éléments que nous pourrons ensuite manipuler librement.

```javascript
render-a//collecte de l'ensemble des élément <a> (hyperliens)
var allLinks = document.getElementsByTagName("a");
console.log(allLinks);

//parcours de l'ensemble des liens pour injecter un comportement différent au click
for (var i = 0; i < allLinks.length; i++) {
    //on utilise une variable temporaire pour simplifier l'écriture
    var link = allLinks[i];

    //pour accéder tout de suite à l'interaction avec le lien et l'annuler, on utilise "mousedown"
    link.addEventListener("mousedown", function(event){
        //récupère le "host" actuel (le serveur qui héberge la page)
        var host = event.target.host;
        console.log("this link host is :", host);

        //récupère l'URL complète de destination, composée à l'aide de "host" lorsque l'URL est relative
        var href = event.target.href;
        console.log("this link href is :", href);

        //écrase le lien (href) pour annuler tout chargement de la nouvelle URL
        var innerLink = href.replace("http://"+host, "");
        event.target.href = "#";
        console.log(event.target);

        //donne une information à l'utilisateur
        alert("you should go to " + innerLink);
        console.log(allLinks);
  });
}
```

**NB :** Pour ce programme, pensez à ouvrir votre console pour mieux comprendre ce qu'il se passe lorsque l'on clique sur un lien!

[Retour à l'index](index.html)

Fin.{style="opacity:0"}
