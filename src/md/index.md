# Mémo JavaScript

Mémo JS est un ensemble de ressources concernant la programmation avec le langage JavaScript, dans le contexte des technologies du Web.

Il s'agit d'une forme écrite et consultable de certains cours proposés par Dominique Cunin dans le cadre de ses enseignements à [l'École Supérieure d'Art de Valence](http://esad-gv.fr){target=blank}.

Cette initiative fait suite à une requête des étudiants, qui désiraient pouvoir accéder à des ressources en dehors des cours et des entretiens individuels, sans qu'il s'agisse pour autant de cours en ligne tiers.

Un investissement plus important dans ce format a été fourni à l'occasion de la période de confinement imposée à la France et à de nombreux pays du monde pendant la pandémie du Coronavirus de 2019-2020.

Télécharger le fichier zip de la version actuelle [ICI](./MemoJS.zip)

## Liste des épisodes

* [Contre l'effet page blanche](?mdpath=md/pageBlanche.md)
* [Où placer son script](?mdpath=md/whereToScript.md)
* [Les conditions : if](?mdpath=md/if.md)
* [De l'aléatoire](?mdpath=md/randomColor.md)
* [Les événements souris](?mdpath=md/mouseEvent.md)
* [Les événements clavier](?mdpath=md/keyboardEvent.md)
* [Les événements clavier #alt : accumuler les lettres dans une chaîne de caractères](?mdpath=md/keyboardEventAlt.md)
* [RebonDiv](?mdpath=md/rebondiv.md)
* [Boucle de répétition : for](?mdpath=md/for.md)
* [Les tableaux : Array](?mdpath=md/array.md)
* [Multi-RebonDiv](?mdpath=md/multi-rebondiv.md)
* [Affichage et programmation du texte](?mdpath=md/stringBasics.md)
* [Convertir un texte en une suite de span](?mdpath=md/text2span.md)
* [Image-Map](?mdpath=md/imageMap.md)

TODO next :
* Load external files
* "real" drag & drop of element

## Crédits

L'ensemble du projet est librement accessible ici :
https://gitlab.com/dominique.cunin/memosjs.

Ceci concerne aussi le "moteur" qui permet d'utiliser les codes sources présentés dans les fichiers markdown afin de les injecter et de les exécuter dans une iFrame. L'idée de ce développement "fait maison" est de ne pas créer de dépendance avec une plateforme comme [codepen](https://codepen.io/), qui, malgré toutes leurs qualités, pourraient disparaître d'un moment à l'autre. Aussi, dans le contexte d'une consultation hors-ligne, l'ensemble des scripts nécessaires au bon fonctionnement du Mémo sont embarqués dans le projet lui-même, facilitant sa transmission sous la forme d'une archive zip, par exemple, pour une exécution en local.

Les composants logiciels suivants sont utilisés :

- markdown-it : [https://github.com/markdown-it/markdown-it](https://github.com/markdown-it/markdown-it)
- markdown-it-attrs.browser : [https://github.com/arve0/markdown-it-attrs](https://github.com/arve0/markdown-it-attrs)
- hightlightjs : [https://github.com/highlightjs/highlight.js](https://github.com/highlightjs/highlight.js)

Que leurs auteurs soient ici chaleureusement remerciés.

