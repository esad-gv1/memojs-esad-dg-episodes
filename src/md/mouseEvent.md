[Retour à l'index](index.html)
# Mémo JavaScript

## Événement souris : faire apparaître des éléments à l'emplacement de la souris

Un grand classique : à chaque déplacement de la souris, un élément graphique est crée aux coordonnées de celle-ci.

De nombreuses formes d'interactivité (ou encore du design des interactions) peuvent être construites avec la souris, qui est un périphérique de pointage et d'interaction devenu banal, mais qui est plus riche et complexe qu'il en à l'air.

### Comment s'y prendre ?

Découpons d'abord le problème en plusieurs sous-problèmes.

1. La souris ? Comment obtenir des informations provenant de ce prériphérique ?

    - Éléments techniques
        - La notion d'écouteur va nous aider ici.
        - On peut associer un écouteur à tous les éléments présents dans la page, y compris à la page elle-même, et donc aussi à la fenêtre!
        - La position de la souris dans la page est représenté par des coordonnées ```x, y```, qui pourront être appliquée à un élement quelconque.

2. Comment faire apparaître de nouveaux éléments dans la fenêtre

    - Éléments techniques
        - Créer une nouvelle balise HTML (c'est à dire des éléments HTML) à chaque événement souris adéquat.
        - Cette balise peut être de n'importe quel type : ```<img> <div> <canvas> <video>``` etc.
        - On peut appliquer un style à chaque nouvel élément pour spécifier son affichage (et sa mise en page).
        
    - Éléments de design
        - S'il s'agit d'images, que représentent-elles ?
        - Quelles sont leurs dimensions ?
        
Les spécifications de ce projet sont assez incomplètes, car elles ne précisent pas quels sont les contenus utilisés et surtout ce pour quoi ils sont utilisés. En d'autres termes, le **propos du projet n'est pas donné, sa dimension esthétique est inconnue**. Il faudrait, bien sûr, qu'elle le soit, sans quoi le projet se résume à un simple exercice technique.

Mais nos petites démonstrations ne prétendent pas à faire œuvre : à vous de mettre dans sens dans les choix des éléments affichés, leur mode d'apparition, le tout en lien avec l'interaction utilisée. C'est cet ensemble technico-esthétique que l'on nommera du **design d'interaction**, ou encore du **design de l'interactivité**.

### Principes techniques en JavaScript

En faisant un effort de traduction de ce que nous avons observé plus haut, les briques techniques dont nous avons besoin sont les suivantes :

- Créer des balises ```<div>``` (pour l'instant) à chaque événement souris :

```javascript
var div = document.body.createElement("div");
```

- Si cette balise est une image ```<img>``` ira chercher la srouce image dans un dossier :

```javascript
var img =  document.body.createElement("img");
img.src = "url/to/image/file.extension"; //attention, événement asynchrone! L'image apparaîtra avec un délai
```
            
- Appliquer un style à chaque nouvelle image :

```javascript
div.style.position = "absolute";
div.style.left = "10px";
div.style...
```
        
- Pour produire l'interactivité, le programme doit accéder aux coordonnées de la souris à tous moments :

```javascript
window.addEventListener("mousemove", callback);
```

De nombreux événements sont produit par la souris et peuvent être écoutés, [une liste est disponible](https://developer.mozilla.org/fr/docs/Web/API/MouseEvent){target=blank}. Ici, nous utilisons ```mousemove``` qui permet d'écouter l'état de la souris à chacun de ses déplacements dans l'espace de la page, ce qui semble le plus adapté à notre projet.

Lors de la génération de l'évenement, on obtient les propriétés [MouseEvent.clientX](https://developer.mozilla.org/fr/docs/Web/API/MouseEvent/clientX){target=blank} et [MouseEvent.clientY](https://developer.mozilla.org/fr/docs/Web/API/MouseEvent/clientY){target=blank} qui nous fournissent les coordonnées de la souris relativement à la page, si l'écouteur est placé sur window.

### Quelques détails en plus

Pour rendre l'exercice un peu plus intéressant, nous ajoutons une selection alératoire de couleur pour la div lors de sa création, et nous faisons attention au positionnement de cette div.
En effet, si l'on applique les coordonnées de la souris directement à la div, un léger décalage apparaît :

```css
render-brender-chtml, body {
    margin: 0px;
    width: 100%;
    height: 100%;
}

div { /* générique tag html */
    width: 100px;
    height: 100px;
    border: solid 1px black;
    position: absolute;
}
```
```javascript
render-bwindow.addEventListener("mousemove", callback);

//la fonction de retour (callback) contient le bloc de code qui sera exécuté lors de l'événement
function callback(event) {

    var x = event.clientX;
    var y = event.clientY;

    //variable représentant la div que l'on crée (facilite l'écriture)
    var tempDiv = document.createElement("div");
    document.body.appendChild(tempDiv); //ajout de la div au body
    
    //définition de la position de la div
    tempDiv.style.left = x + "px";
    tempDiv.style.top = y + "px";
}
```

La résolution de ce "problème" est assez simple. Si l'on souhaite que la ```div``` nouvellement créée apparaîssent au centre de la souris, alors il faut décaler cette div pour la "center".
En termes techniques, il faut soustraire aux coordonnées de la souris la moitié de la largeur et de la hauteur de la div pour obtenir la bonne position : 

```javascript
render-cwindow.addEventListener("mousemove", callback);

//la fonction de retour (callback) contient le bloc de code qui sera exécuté lors de l'événement
function callback(event) {

    var x = event.clientX;
    var y = event.clientY;

    //on donne ici les dimensions de la div
    var width = 100;
    var height = 60;

    //variable représentant la div que l'on crée (facilite l'écriture)
    var tempDiv = document.createElement("div");
    document.body.appendChild(tempDiv); //ajout de la div au body
    
    //on applique les dimensions dans le style de la div
    tempDiv.style.width = width + "px";
    tempDiv.style.height = height + "px";

    //définition de la position de la div au "centre de la souris"
    tempDiv.style.left = x - width/2 + "px";
    tempDiv.style.top = y  - height/2  + "px";
}
```


## Code source complet

```css
render-ahtml, body {
    margin: 0px;
    width: 100%;
    height: 100%;
}

div { /* générique tag html */
    width: 100px;
    height: 100px;
    background-color: green;
    border: solid 1px black;
    position: absolute;
}
```


```javascript
render-awindow.addEventListener("mousemove", callback);

//la fonction de retour (callback) contient le bloc de code qui sera exécuté lors de l'événement
function callback(event) {
    var x = event.clientX;
    var y = event.clientY;

    //variable représentant la div que l'on crée (facilite l'écriture)
    var tempDiv = document.createElement("div");
    document.body.appendChild(tempDiv); //ajout de la div au body
    
    //définition de la couleur de fond
    tempDiv.style.backgroundColor = getRandomRGBColor();

    //définition des dimensions
    var width = getRandomFromTo(20, 50);
    var height = getRandomFromTo(20, 50);

    //application au rendu graphique
    tempDiv.style.width = width + "px";
    tempDiv.style.height = height + "px";

    //définition de la position de la div
    tempDiv.style.left = x - width/2 + "px";
    tempDiv.style.top = y - height/2 + "px";

    //on change l'arrondi de la div et son opacité
    tempDiv.style.borderRadius = getRandomFromTo(10, 100) + "%";
    tempDiv.style.opacity = getRandomFromTo(0, 1);

    //indice pour rendre aléatoire la couleur du fond de la page!
    //document.body.style.backgroundColor = getRandomRGBColor();
}

//la fonction randomFromTo prend 2 arguments qui seront utilisés pour effectuer le calcul de tirage aléatoire.
function getRandomFromTo(from, to) {
    var result = Math.random() * (to - from) + from;
    return result; //on retourne la valeur de la variable result
}

//Récupère une couleur CSS aléatoire sur les 3 canaux de couleur rgb
function getRandomRGBColor() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}