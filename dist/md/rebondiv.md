[Retour à l'index](index.html)
# Mémo JavaScript

## Faire rebondir un élément graphique sur les bords de l'écran

Si l'interactivité repose sur la possibilité technique qu'une interaction avec la machine soit saisie pour être rejouée par l'intermédiaire d'un programme (ex  : un événement de touche de clavier permet d'afficher du texte ou de déclencher un son), il ne faut pas limiter la notion d'interaction au seul dialogue humain-machine. En effet, des interactions peuvent avoir lieu dans un programme sans qu'un agent extérieur n'intervienne.
On dit des interactions qui sont provoquées par une source extérieure qu'elles sont **exogènes**. À l'inverse, celles qui ne reposent que sur des processus internes sont dites **endogènes**.

Ici, nous souhaitons mettre en place un projet très simple : un élément graphique quelconque se déplace dans l'écran et rebondit sur les bords de la fenêtre.

L'élément graphique doit donc interagir avec son environnement d'affichage pour que notre projet puisse se réaliser. Il s'agit donc ici d'un projet interactif, mais uniquement de manière endogène.

### Comment s'y prendre ?

Spécifions plus clairement le projet, aussi simple soit-il.

#### Un élément graphique se déplace dans la fenêtre ?

Un espace d'affichage numérique est organisé en une suite d'éléments discrets (c'est-à-dire clairement séparés les uns des autres), les *pixels* (picture elements). Afin d'être manipulables simplement, ces pixels sont organisés de manière rationnelle pour former une grille à deux dimensions (x, y). Chaque pixel a donc une coordonnée précise.

Les systèmes logiciels permettant le dessin de graphismes dans l'écran utilisent des abstractions pour faciliter l'écriture de programmes. Une forme visuelle est donc généralement définie en tant que forme géométrique : elle est composée de points (sommets) reliés entre eux par des segments afin de tracer un contour, qui peut ensuite être rempli de couleurs.

Ces formes géométriques sont dessinées automatiquement par les couches logicielles inférieures des systèmes d'exploitation. Néanmoins, le principe d'organisation des pixels dans l'écran ne disparaît pas pour autant : on place une forme graphique dans son espace d'affichage en transformant ses coordonnées géométriques (x, y, z).

Plusieurs contextes graphiques existent dans les technologies du Web : HTML, SVG, Canvas 2D, WebGL. Dans un document HTML, les éléments (ou nœuds) visuels sont automatiquement positionnés relativement à la page dans laquelle ils se trouvent. Ceci implique qu'une certaine mise en page est prédéterminée pour les éléments HTML qui viennent s'ajouter au document, comme le montre l'exemple ci-dessous.

```javascript
render-avar div = document.createElement("div");
div.style.width = "100px";
div.style.height = "100px";
div.style.border = "solid 1px black";
document.body.appendChild(div);

var div2 = document.createElement("div");
div2.style.width = "100px";
div2.style.height = "100px";
div2.style.border = "solid 1px black";
div2.style.left = "200px";
div2.style.top = "150px";
document.body.appendChild(div2);
```
On remarque ici que même si l'on accède aux propriétés de style (css) de l'une des ```<div>``` pour changer sa position dans l'espace de la fenêtre, rien n'est appliqué à l'élément, qui est placé automatiquement dans la page. 

Il faut alors permettre à l'élément de sortir de cette mise en page et afin d'être placé aux coordonnées qu'on lui donne. La propriété de style ```position``` ([référence](https://developer.mozilla.org/fr/docs/Web/CSS/position){target=blank}) permet cela. Nous devons passer d'une position qui est décidée relativement au nœud parent de l'élément, à une position absolue dans l'espace de la fenêtre.

```javascript
render-avar div = document.createElement("div");
div.style.width = "100px";
div.style.height = "100px";
div.style.border = "solid 1px black";
document.body.appendChild(div);

var div2 = document.createElement("div");
div2.style.position = "absolute"; //on passe en coordonnées absolues
div2.style.width = "100px";
div2.style.height = "100px";
div2.style.border = "solid 1px black";
div2.style.left = "200px"; //on change la coordonnée en x
div2.style.top = "150px"; //et en y 
document.body.appendChild(div2);
```
À partir de cet exemple, une partie des éléments techniques sont trouvés : pour placer arbitrairement un élément dans la page il faut transformer ses coordonnées, et donc libérer l'élément de la mise en page par défaut. Techniquement, il nous faut passer par les propriétés de style de positionnement de l'élément.

#### Le déplacement dans la fenêtre ?

Il nous faut maintenant mettre en place un système qui transforme les coordonnées de l'élément au fil du temps.

Pour répéter une séquence de programme, une solution technique est d'utiliser ```requestAnimationFrame();```([référence](https://developer.mozilla.org/fr/docs/Web/API/window/requestAnimationFrame){target=blank}).

```javascript
function update()
{
    //le code source de ce bloc est exécuté aussi rapidement que le navigateur saura le faire
    requestAnimationFrame(update);// en appelant à nouveau l'animation dans la fonction, la mise à jour se fait en continue
}

requestAnimationFrame(update);
```

Pour faire se déplacer une ```div``` dans la fenêtre, il est donc possible d'utiliser une variable représentant la coordonnée à transformer et l'appliquer au style de l'objet.

```javascript
//construction de la scène
var div = document.createElement("div");
div.style.position = "absolute";
div.style.width = "100px";
div.style.height = "100px";
div.style.border = "solid 1px black";
document.body.appendChild(div);

var x = 0; //variable représentant la coordonnée x

//fonction utilisée pour le rafraîchissement du programme
function update()
{
    x = x + 1; //ajout de la valeur 1 à la valeur actuelle de x
    div.style.left = x + "px";
    requestAnimationFrame(update);
}
//lancement du rafraichissement à l'aide de setInterval
requestAnimationFrame(update);
```
Ici, la fréquence de rafraîchissement est aussi rapide que ce que le navigateur est capable de faire.

#### Conditionner (limiter) le déplacement dans la fenêtre ?

Pour éviter que l'élément graphique ne sorte de la zone d'affichage, il faut réussir à stopper le processus d'augmentation de la valeur de coordonnée. Une condition permet de limiter l'effet d'une instruction ou d'un bloc d'instruction. Le principe est le suivant :

```
SI
  la coordonnée en x est inférieure à la largeur de la fenêtre
ALORS
  augmenter la coordonnée
```

```javascript
if(x < window.innerWidth)
{
  x += 1; //écriture alternative équivalente à x = x +1
  div.style.left = x + "px";
}
```

Mais le résultat n'est ici pas très satisfaisant, car la ```div```, si elle se déplace bien dans la fenêtre, ne "rebondis" par sur le bord de l'écran. Il y a donc un problème de conception dans ce programme.

Une solution serait d'utiliser une autre valeur qui pourrait basculer d'un état à un autre lorsque les coordonnées atteignent les limites de la fenêtre. Cette valeur représenterait alors la vitesse qui serait appliquée aux coordonnées de la ```div```. Cette vitesse, exprimée sous la forme d'un nombre, pourrait alors être positive ou négative, ce qui correspondrait à un déplacement dans un sens, ou dans l'autre. Le basculement du positif vers le négatif, et vice-versa, serait fait lorsque l'objet graphique dépasse les frontières de la fenêtre. Si l'on tente de décrire ce principe plus précisément, on arriverait à :

```javascript
vitesse_en_x = 1

SI
  la coordonnée en x est supérieure à la largeur de la fenêtre
ALORS
  vitesse_en_x = -vitesse_en_x //la nouvelle valeur de la variable est l'inverse de sa valeur actuelle!
  
augmenter la coordonnée de vitesse_en_x
```

### VOILÀ!

Tous les éléments techniques du programme sont réunis :
- Transformation des coordonnées d'un élément graphique
- Déplacement dans l'espace de la fenêtre
- Conditionnement du déplacement aux bords de la fenêtre (rebond)

 En les assemblant les uns avec les autres, on obtient le programme complet ci-dessous :

```javascript
render-a//construction de la scène
var x = 0;
var y = 0;
var speedX = 2;
var speedY = 3;

var width = 100;
var height = 100;

var div = document.createElement("div");
div.style.position = "absolute";
div.style.width = width + "px";
div.style.height = height + "px";
div.style.border = "solid 1px black";
document.body.appendChild(div);

function update()
{ 
    //suite de conditions pour gérer la vitesse et donc le rebond
    if(x > window.innerWidth-width)
    {
      speedX = -speedX;
      x = window.innerWidth-width; //on contraint absolument la coordonnée à la limite de la fenêtre
    }
    if(x < 0)
    {
      speedX = -speedX;
      x = 0; //on contraint absolument la coordonnée à la limite de la fenêtre
    }
    
    if(y > window.innerHeight-height)
    {
      speedY = -speedY;
      y = window.innerHeight-height; //on contraint absolument la coordonnée à la limite de la fenêtre
    }
    if(y < 0)
    {
      speedY = -speedY;
      y = 0; //on contraint absolument la coordonnée à la limite de la fenêtre
    }
    
    //on applique la vitesse (toujours après les conditions)
    x += speedX;
    y += speedY;
    
    //on applique à l'élément graphique (toujours après les calculs)
    div.style.left = x + "px";
    div.style.top = y + "px";

    requestAnimationFrame(update);
}

//répétition au fil du temps
requestAnimationFrame(update);
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}