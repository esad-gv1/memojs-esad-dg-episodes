[Retour à l'index](index.html)
# Mémo JavaScript

## Ressource à télécharger

Un projet pouvant servir de base de travail peut-être téléchargé ici : [projet de base](rsrc/files/base_projet.zip)

## Où placer son script ?

### Rappel : les trois fichiers

Nous utilisons une structure qui sépare en trois fichiers les trois types de sources que nous utilisons, qui sont écrites avec trois langages différents.

1. ```index.html```, description en langage HTML du document
2. ```style.css```, définition en langage CSS du style de la page et de ses éléments.
3. ```script.js```, définition en langage JavaScript des comportements programmés

Concernant JavaScript, l'exécution du contenu du fichier, c'est-à-dire le programme en tant que tel, est effectuée lorsque la balise HTML permettant le chargement du fichier est atteinte.

Prenons deux exemples : 

```html
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf8">
    <link type="text/css" href="style.css" rel="stylesheet">
  </head>

  <body>
    <script src="script.js"></script>
  </body>
  
</html>
```
Ici, le fichier ```script.js``` est chargé dans la balise ```<body>```. Donc une instruction (une ligne de code) qui fera appel au body sera exécutée sans problème :

```javascript
var div = document.createElement("div");
document.body.appendChild(div);
```
La ```div``` sera bien ajoutée au ```<body>```, car le script est exécuté depuis l'intérieur de l'élément ```<body>```, nous avons donc l'assurance que ce dernier existe.

```html
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf8">
    <link type="text/css" href="style.css" rel="stylesheet">
    <script src="script.js"></script>
  </head>

  <body>
  </body>
  
</html>
```
Différence importante ici : le script est chargé dans l'élément ```<head>```, qui est interprété en premier dans le document, avant le ```<body>```.

Donc, si nous exécutons les mêmes instructions que précédemment, nous aurons une erreur!

```javascript
var div = document.createElement("div");
document.body.appendChild(div);

//dans la console : TypeError: null is not an object (evaluating 'document.body.appendChild')
```
En effet, lors de l'exécution du script, qui se fait depuis l'élément ```<head>```, le ```<body>``` **n'existe pas encore**.

## Chargement du script dans ```<head>```

Si nous voulons tout de même charger notre script dans le ```<head>```, il suffit de conditionner l'exécution du programme qui doit utiliser le ```<body>``` de sorte que le chargement du document HTML soit terminé intégralement.

Ici encore, les événements vont nous aider. L'objet ```window``` génère un événement de type ```"load"``` lorsque le document est entièrement chargé. Il est donc possible de **n'exécuter un fragment de programme qu'une fois le chargement de la page terminé** :

```javascript
window.addEventListener("load", setup);

function setup() {
    var div = document.createElement("div");
    document.body.appendChild(div);
}

//cette fois, pas d'erreur.
```
```setup``` est une fonction appelée en retour (callback) du déclenchement d'un événement. Ici, lorsque la page est chargée, ```"load"``` est envoyé à l'écouteur d'événement qui exécute la fonction donnée en référence : ```setup```.

**NB : Dans l'ensemble des épisodes de ce MémoJS, pour des raisons de clarté et de fonctionnement technique des encarts interactifs présentant le résultat des codes sources, c'est la première méthode qui est systématiquement utilisée.**

Votre fichier HTML doit alors est structuré ainsi :

```html
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf8">
    <link type="text/css" href="style.css" rel="stylesheet">
  </head>

  <body>
    <script src="script.js"></script>
  </body>
  
</html>
```
Et les codes sources seront écrits dans le fichier ```script.js```.


[Retour à l'index](index.html)

Fin.{style="opacity:0"}