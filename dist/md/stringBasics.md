[Retour à l'index](index.html)
# Mémo JavaScript

## Programmer l'affichage du texte (avec le DOM)

Si l'organisation d'éléments textuels dans un espace d'impression fait partie des compétences du designer graphique, alors il devrait probablement en aller de même avec les espaces d'affichage numériques et interactifs.

Comment afficher, gérer, générer, contraindre, effacer, organiser des éléments textuels dans une page Web ? Plus précisément, quels sont les outils que la programmation met à notre disposition pour faire du "traitement" de texte ?

Ici il ne s'agit pas de revenir sur l'histoire de la mise en page, de ses évolutions au cours de l'histoire de l'écriture, des supports imprimés et des espaces d'affichage en général, quelles que soient la technique et la technologie employée.

Il est question d'aborder les principes fondamentaux de l'affichage et de la programmation de texte à l'aide de JavaScript. Il est à noter que ce qui est exposé ici garde une certaine validité dans d'autres langages de programmation.

## Afficher des morceaux de phrases aléatoirement dans la page

Notre projet sera simple, comme toujours. Une description intuitive en serait :

**Faire s'afficher des morceaux de phrases à des endroits aléatoires dans la page.**

### Comment s'y prendre ?

Après cette première phase d'énonciation, entrons dans les détails, en listant nos questions, mais aussi ce que nous savons déjà :

  - Afficher du texte
    - Beaucoup d'éléments HTML peuvent afficher du texte.
    - CSS permet de définir le style du texte : police de caractère, taille, corps, interlignage, etc.
    - JavaScript permet d'accéder au style d'un élément à l'aide de la propriété ```style```.


  - Programmer du texte
    - Le contenu d'un élément HTML peut-être défini à l'aide de la propriété ```innerText```, ```innerHTML```, ou encore ```textContent```, mais attention, les trois ne sont pas équivalents!
    - [innerHTML](https://developer.mozilla.org/fr/docs/Web/API/Element/innertHTML){target=blank} VS [innerText](https://developer.mozilla.org/fr/docs/Web/API/Node/innerText){target=blank} VS [textContent](https://developer.mozilla.org/fr/docs/Web/API/Node/textContent)
    - Comment modifier le texte contenu dans un élément HTML par le programme ? JavaScript doit contenir une réponse...

  - Déclencher l'affichage
    - Nous connaissons les événements : clavier, souris (```element.addEventListener("type", callback)```).
    - Choisissons la souris pour cette fois : au click, un morceau de phrase apparaîtra.

  - Morceaux de phrases ?
    - Est-ce qu'il s'agit d'un mot ? D'une lettre ? Quel est le plus petit élément que nous voulons considérer ?
    - Quel que soit notre choix : un élément HTML contenant le fragment de texte doit apparaître, c'est-à-dire être généré, rempli du texte souhaité puis ajouté dans le document courant (dans l'arborescence HTML) (pour rappel [génération et ajout dynamique d'un élément HTML via JavaScript](?mdpath=md/rebondiv.md){target=blank})
    - Comment manipuler du texte en programmation ?

### Principes techniques en JavaScript

Beaucoup de questions font surface dans la 2e phase d'énonciation du projet. Cherchons leurs réponses.

#### Strings

Dans l'épisode concernant les [interactions clavier](?mdpath=md/keyboardEvent.md){target=blank}, nous avons abordé les encodages numériques des caractères, en évoquant les formats ASCII et Unicode. Nous savons donc qu'un caractère (une lettre ou un symbole) est représenté par un nombre : ```A``` vaut ```65```, ```a``` vaut ```97```, etc. selon un long tableau de correspondances standardisé.

Nous savons aussi que nous pouvons stocker des informations de **type "nombre"** dans des variables pour les manipuler et les utiliser tout au long du programme.

```javascript
var unNombre = 65; // une variable contenant la représentation d'un nombre
```

Il nous faut découvrir un autre **type de données** utilisable dans les programmes : **les chaînes de caractères** :

```javascript
var uneLettre = "a"; // une variable contenant la représentation d'un caractère

var desLettres = 'abcd'; // une variable contenant la représentation d'une suite (une chaîne) de catactères
```
Pour définir une chaîne de caractères, c'est-à-dire un ensemble de données qui représente des lettres et des symboles qui devront être dessinés comme étant du texte, on utilise le type **String** ([référence de l'objet String](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String){target=blank}).

Comme on le voit ci-dessus, ces données sont identifiables dans le programme à l'aide des guillemets doubles, ```"texte"```, ou simples ```'texte'```.

Une String est donc une suite de caractères ou de symboles qui ont vocation a être affichés. Pour que cet affichage soit effectif, le **caractère** sera lié à un **glyphe**, c'est-à-dire un dessin du caractère qui permettra son affichage. Il existe plusieurs formats de description de ces dessins ([TrueType](https://developer.apple.com/fonts/TrueType-Reference-Manual/){target=blank}, [OpenType](https://docs.microsoft.com/en-us/typography/opentype/spec/){target=blank}, etc.), chacun ayant son histoire et ses caractéristiques, mais ceci dépasse assez largement le propos de cet épisode.
    
>Notons que le dessin à proprement parler des caractères à l'écran est géré par le système d'exploitation. C'est même l'un des composants fondamentaux des librairies logicielles graphiques qui permettent aux interfaces graphiques utilisateurs d'être ce qu'elles sont.

Exemple : ```65``` est le code décimal de la lettre ```"A"```, c'est donc la cellule 65 qui sera utilisée dans le fichier de la police de caractère ( *font* ) pour aller chercher le tracé de la lettre A et la dessiner dans l'écran, comme le montre ici [FontForge](https://fontforge.org/en-US/){target=blank} :

![](assets/img/A.png)

#### Utilisation des Strings

Une chaîne de caractères peut contenir un nombre de caractères variables : il est possible d'ajouter et d'enlever des éléments dynamiquement dans la chaîne.

```javascript
var uneLettre = "a";
var desLettres = uneLettre + "b";

// résultat : desLettres = "ab"
```
L'opérateur ```+``` "comprend" qu'il s'agit d'ajouter des caractères entre eux, il ne fait donc pas une addition entre des nombres, mais une *concaténation* entre les deux caractères.

```javascript
console.log( 1 + 1 );
// résultat : 2

console.log( 1 + "1" );
// résultat : "11"
```
Une **converstion implicite** est réalisée par l'opérateur d'addition lorsqu'une String est utilisée dans l'opération : ici, le nombre ```1``` est automatiquement converti en String ```"1"```. C'est ce que nous avons déjà vu dans [rebonDiv](?mdpath=md/rebondiv.md){target=blank}), pour modifier les coordonnées des div et les positionner dans la page ( ```x + "px"``` ).

Pour connaître le **nombre de caractères dans une chaîne**, on utilise la propriété ```length```

```javascript
var phrase = "Ceci est un texte à afficher";

console.log( phrase.length );
// résultat : 28
```

Pour accéder à un caractère en particulier dans la chaine, on utilise un index qui commence par 0 et qui ne peut dépasser le nombre maximum de caractères contenus dans la chaîne (autrement dit, le résultat de ```string.length```).

On peut utiliser deux méthodes : l'usage de crochet ```[]``` ou la méthode ```string.charAt(index)``` : [référence de charAt()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/charAt){target=blank}.

```javascript
var phrase = "Ceci est un texte à afficher";

console.log( phrase[6] + " ou bien " + phrase.charAt(6) );
// résultat : "s ou bien s"
```
Bien sûr, un espace compte comme étant un caractère, c'est pourquoi dans l'exemple ci-dessus, il faut compter ainsi :

|C|e|c|i| |e|s{style="color:red"}|t|
|-|-|-|-|-|-|-|-|
|0|1|2|3|4|5|6{style="color:red"}|7|

#### Passer une chaîne de caractères dans un élément HTML

Mise en pratique : utiliser la propriété ```innerText``` de l'élément HTML pour lui transmettre la chaîne de caractères à afficher.

```javascript
render-avar phrase = "Ceci est un texte à afficher";

var div = document.createElement("div");
div.innerText = phrase; //la chaîne contenue dans la variable 'phrase' est transmise à la div pour affichage
div.style.fontSize = "30px"; //un peu de css
div.style.fontFamily = "sans-serif"; //un peu de css

document.body.appendChild(div);
```

### Ajouter un caractère au click

Faisons une première version de notre programme avec ce que nous avons accumulé comme connaissances jusqu'à présent. En langage naturel, voici ce que nous obtenons :

- Générer une div qui sera ajoutée au ```body``` du document.

- Mettre en place un écouteur d'événements pour capturer les clicks de la souris (ou du trackpad) dans la fenêtre. Dans la fonction gestionnaire (ou callback) exécutée à chaque click, nous ajoutons un caractère de plus au contenu de la div précédemment créée.

- Une variable représentant l'index du caractère à afficher sera incrémentée à chaque click. Elle servira dans la méthode ```charAt()```. Ainsi, avec la déclaration```var index = 0``` en dehors de la callback, puis ```ìndex++``` dans la callback, on obtiendra bien une suite de nombres : ```0, 1, 2, 3, ...```

- Une chaîne de caractère de référence, conservée dans une variable globale, nous permettra d'aller y chercher les caractères à ajouter les uns après les autres, du premier jusqu'à dernier, dans la div.

Profitons de l'occasion pour comparer l'usage de ```innerText``` et ```textContent```. **Pour tester, cliquez dans le cadre sous le code source**. Est-ce que vous comprenez la différence ?

```javascript
render-b//ajout de l'écouteur sur la fenêtre
window.addEventListener("click", clickHandler);
//chaîne de référence
var phrase = "Ceci est un texte à afficher";

//div à afficher puis remplir
var divInnerText = document.createElement("div");
divInnerText.style.fontSize = "30px"; //un peu de css
divInnerText.style.fontFamily = "sans-serif";
document.body.appendChild(divInnerText);

var divTextContent = document.createElement("div");
divTextContent.style.fontSize = "30px"; //un peu de css
divTextContent.style.fontFamily = "sans-serif";
document.body.appendChild(divTextContent);

//index à utiliser pour choisir le caractère à ajouter au click
var charIndex = 0;

//gestionnaire de l'événement click
function clickHandler() {

    //on récupère le caractère courant dans une variable temporaire
    var char = phrase.charAt(charIndex);
    //et on ajoute une div contenant ce caractère
    //via innerText
    divInnerText.innerText += char;
    //via textContent
    divTextContent.textContent += char
    //On incrémente l'index à utiliser à la prochaine itération
    charIndex++;
}
```

### Positionnement aléatoire dans la page ?

Le projet n'est pas encore terminé. En effet, nous avons énoncé le positionnement aléatoire de fragments de texte dans la page. Mais, ici, nous avons rempli des div de texte, caractère par caractère, pour reconstituer une phrase "classique".

Il nous faudrait donc réussir à afficher chaque caractère indépendamment les uns des autres, à une position aléatoire dans la page.

Nous savons libérer les éléments HTML de la mise en page par défaut, à l'aide de la régle CSS ```position``` avec la valeur ```"absolute"```.
Nous savons aussi changer leur coordonnées dans la page en passant par les règles ```left``` et ```top```, sans oublier de donner une unité à la valeur (```px``` ou autre).

Il nous manque donc le positionnement aléatoire des éléments générés à la volée, lors du click.

JavaScript contient nativement tout un ensemble de méthodes permettant d'effectuer des calculs mathématiques : [Math](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math){target=blank}. Celle qui nous intéresse porte bien son nom : [random](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/random){target=blank}.

Cette méthode peut être utilisée en l'état, mais cela ne correspond pas complètement à nos besoins. ```Math.random()``` retourne un nombre entre 0 et 1. Il nous serait ici utile, pour notre tirage aléatoire, de pouvoir définir une valeur minimum et une une valeur maximum entre lesquelles un nombre doit être choisi au hasard.

Utilisons une fonction pour résoudre ce problème, comme nous l'avons déjà vu dans le premier épisode [Contre l'effet page blanche](?mdpath=md/pageBlanche.md){target=blank}.

```javascript
//fonction utilitaire : tirage au hasard d'un nombre entre deux valeurs
function randomFromTo(from, to) {
    return Math.random() * (to - from) + from;
}

console.log( randomFromTo(10, 50) );
//résultat : un nombre à virgule entre 10 et 50, 11.68, 38.65, etc.
```

### Ce qu'il manque encore

La plus grande part des éléments techniques sont réunis. Le projet est pratiquement terminé. Il faut parfaire son **implémentation**, en séparant les composants techniques dans des fonctions lorsque c'est pertinent, et en prenant garde à ne pas faire s'exécuter des instructions lorsque c'est inutile, ici, en l'occurrence, il faut contraindre l'ajout de nouvelles div au nombre de caractères présents dans la chaîne de référence.

Voici une proposition d'implémentation. De nombreuses variantes sont possibles, mais c'est à vous de les découvrir!

## Code source complet

Cliquer dans le cadre, en dessous du code source, pour voir le résultat.

```javascript
render-c//ajout d'un écouteur d'événement "click" sur la fenêtre
window.addEventListener("click", clickHandler);

//notre texte à afficher caractère par caractère
var baseText = "Ceci est un texte à afficher dans la page. La position de chaque lettre est tirée au hasard. Il faut cliquer dans la fenêtre pour déclencher l'apparition d'une nouvelle lettre.";

//index courant du caractère à afficher
var charIndex = 0;

//gestionnaire de l'événement click
function clickHandler() {

    //si l'index courant est inférieur au nombre total de caractères
    //dans le texte
    if (charIndex < baseText.length) {
        //alors on récupère le caractère courant
        var char = baseText.charAt(charIndex);
        //et on ajoute une div contenant ce caractère
        addTextDiv(char);
        //On incrémente l'index à utiliser à la prochaine itération
        charIndex++;
    }
}

//fonction d'ajout d'une div dans la page
// (text) est l'argument passé lors de l'appel de fonction
function addTextDiv(text) {

    //génére la nouvelle div
    var div = document.createElement("div");
    //et la remplie du texte passé en argument de la fonction
    div.innerText = text;

    //on libère la div de la mise en page
    div.style.position = "absolute";
    //on empêche l'effet de surlignage du texte avec la souris
    //lors de la selection
    div.style.userSelect = "none";
    //quelques effets de styles
    div.style.fontFamily = "sans-serif";
    div.style.fontSize = "20px";

    //ajoute la div au body pour rendre l'élément effectif
    document.body.appendChild(div);

    //calcul les nouvelles coordonnées de la div
    var x = randomFromTo(0, window.innerWidth - div.offsetWidth);
    var y = randomFromTo(0, window.innerHeight - div.offsetHeight);

    //application des coordonnées via le style
    div.style.left = x + "px";
    div.style.top = y + "px";
}

//fonction utilitaire : tirage au hasard d'un nombre entre deux valeurs
function randomFromTo(from, to) {
    return Math.random() * (to - from) + from;
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}