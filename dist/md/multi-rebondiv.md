[Retour à l'index](index.html)
# Mémo JavaScript

## Faire rebondir un **grand nombre** d'éléments graphiques sur les bords de l'écran

Pour poursuivre ce qui a été abordé dans [rebondiv](?mdpath=md/rebondiv.md){target=blank}, nous pouvons écrire un programme qui fait rebondir non pas un seul élément dans les limites de la page, mais un nombre plus ou moins grand.

Le principe du projet est donc **exactement le même que dans *rebondiv***, à la seule différence qu'il doit s'appliquer à un nombre variable d'éléments présents à l'écran.

Cet exercice va nous permettre d'aborder plus en détail la mise en œuvre des Array (tableaux, ou listes), qui est un type de données très fréquement utilisé en programmation. Une synthèse sur les [Array est disponible ici](?mdpath=md/array.md){target=blank}.

En termes de **design logiciel**, nous allons revoir aussi à quel point il est important de travailler de manière **itérative**, c'est-à-dire en trouvant progressivement les solutions techniques en faisant une suite de tentatives, pour finalement les réunir dans un programme cohérent.

Note problème technique principal est ici : comment appliquer le même comportement de rebond dans les limites de la fenêtre à de multiples éléments visuels ?

## Comment s'y prendre ?

### Une chose après l'autre

Dans [rebondiv](?mdpath=md/rebondiv.md){target=blank}, nous avions construit notre solution en séparant les deux axes sur lesquels l'élément se déplace ```(x, y)```. Cette manière de procéder nous avait permis de comprendre la démarche technique à appliquer en isolant le minimum des éléments indispensables à notre réflexion. En commençant à chercher une solution uniquement pour l'axe ```x```, nous avons réduit le nombre d'inconnues à notre problème, ce qui facilite aussi sa résolution.

La base de ce nouveau projet est connue, elle provient de notre projet précédent, nous allons l'utiliser pour comprendre ce qu'il nous faut transformer afin d'atteindre notre objectif :

```javascript
//construction de la scène
var div = document.createElement("div");
div.style.position = "absolute";
div.style.width = "100px";
div.style.height = "100px";
div.style.border = "solid 1px black";
document.body.appendChild(div);

var x = 0; //variable représentant la coordonnée x

//fonction utilisée pour le rafraichissement du programme
function update()
{
    x = x + 1; //ajout de la valeur 1 à la valeur actuelle de x
    div.style.left = x + "px";
    requestAnimationFrame(update);
}
//lancement du rafraichissement à l'aide de setInterval
requestAnimationFrame(update);
```

Limitons à un seul axe de déplacement pour mieux comprendre les choses. 

### Des éléments multiples

Comment allons-nous faire pour faire se déplacer chacune de ces ```div``` dans la fenêtre ?

Plusieurs problèmes se posent :

- Comment gérer le déplacement de chacun des éléments, un à un ? Avant, nous avions une variable ```x```, représentant la coordonnée du même nom, que nous faisions évoluer dans le temps en lui ajoutant une valeur. Pouvons-nous faire la même chose ?
- Si nous arrivons à définir autant de coordonnées ```x``` qu'il y a d'éléments, comment les affecter aux éléments ?

La clé se trouve dans l'utilisation des tableaux, ou Array :

## Boucles et gestion des Arrays

Utilisons nos acquis provenant des épisodes précédents pour trouver une résolution :

- Lorsque de nombreuses lignes de code redondantes se répètent de manière **itérative**, il faut étudier la possibilité d'automatiser les processus répétitifs. [La mise en œuvre d'une boucle est souvent une solution](?mdpath=md/for.md){target=blank}.

- Les tableaux permettent de stocker un grand nombre de valeurs dans une variable et d'y accéder par l'intermédiaire d'un index **itératif**.

**Puisqu'un tableau peut être manipulé à l'aide d'une boucle, c'est que nous allons faire**

```javascript
//tableau de stockage des futures div
//!!Important!! Cette déclaration est faite en dehors et en amont de la boucle!
var allDiv = [];

//boucle de génération des div et de stockage dans le tableau
for(var i=0; i < 5; i++) {
    //variable temporaire (n'existe que dans la boucle!)
    var div = document.createElement("div");
    document.body.appendChild(div);
    //stockage dans le tableau
    allDiv[i] = div;
}
```

Dans le tableau, voici ce qu'il se passe :

- Itération 0 :

|0|
|-|
|div|

- Itération 1 :

|0|1|
|-|-|
|div|div|

- Itération 2 :

|0|1|2|
|-|-|-|
|div|div|div|

- Etc.

Nous pouvons donc accéder à tout moment à ce tableau pour y récupérer la référence à l'une des divs créée par la boucle ```for```.

Appliquons maintenant ce même principe à la valeur de coordonnées que nous modifions dans le temps pour faire se déplacer l'élément!

**NB : Pour des raisons de simplicité dans la gestion de la vitesse de rafraichissement, l'exemple ci-dessous utilise exceptionnellement ```setInterval()``` au lieu de ```requestAnimationFrame()```**

```javascript
render-b//tableau de références aux div
var allDiv = [];
//tableau représentant les coordonnées x
var x = [];

//construction de la scène
for(var i=0; i<10; i++) {
    var div = document.createElement("div");
    div.style.position = "absolute";
    div.style.width = "100px";
    div.style.height = "100px";
    div.style.border = "solid 1px black";
    //ajout d'un petit décalage vertical entre chaque div
    div.style.top = i*30 + "px";
    document.body.appendChild(div);

    allDiv[i] = div;
    x[i] = 0;
}

//fonction utilisée pour le rafraichissement du programme
function update()
{
    for(var i=0; i<10; i++) {
        x[i] = x[i] + 1; //ajout de la valeur 1 à la valeur actuelle de x
        allDiv[i].style.left = x[i] + "px";
    }
}
//lancement du rafraichissement
setInterval(update, 1000);
```

### Constats : une réécriture minutieuse, mais simple!

En observant bien les différences entre le programme initial, provenant de [rebondiv](?mdpath=md/rebondiv.md){target=blank}, on constate que les différences sont assez faciles à identifier : toutes les variables qui ne représentaient qu'une seule valeur sont maintenant des tableaux.

Il y a donc un travail d'adaptation à faire entre la version fonctionnant pour un seul élément graphique, et celle fonctionnant pour un nombre quelconque. Il faut transformer les variables qui le doivent en tableau et apprendre à utiliser les boucles ```for```.

Avant d'arriver au code source complet du projet, voici l'une des étapes de cette réécriture : faire rebondir plusieurs éléments sur l'axe x uniquement. Ceci va nous permettre de comprendre le principe d'adaptation du programme avec des tableaux et des boucle.

Voici le programme pour un seul élément :

**NB : Cliquer dans le cadre pour activer/arrêter l'animation!**

```javascript
render-c//pour éviter de faire s'exécuter le script en continu (et surcharger la machine)
//on ajoute un système de on/off au click de souris
var canUpdate = false;

window.addEventListener("click", setUpdate);

function setUpdate() {
    if(canUpdate === true) {
        canUpdate = false;
    } else {
        canUpdate = true;
    }
}

//coordonnées x
var x = 0;
//vitesse en x, qui bascule entre positif et négatif
var speedX = 1;
//acces simplifié à la largeur de l'élément
var width = 100;

//construction de la scène
var div = document.createElement("div");
div.style.position = "absolute";
div.style.width = width + "px"; //on utilise notre variable ici
div.style.height = "100px";
div.style.border = "solid 1px black";
document.body.appendChild(div);

//fonction utilisée pour le rafraichissement du programme
function update()
{
    if(canUpdate) {
        if(x > window.innerWidth - width) {
        speedX = -speedX; // on inverse la direction ici
        }
        if(x < 0) {
        speedX = -speedX; // on inverse la direction ici aussi
        }
        //calcul de la nouvelle coordonnée
        x += speedX;

        //application de la coordonnée
        div.style.left = x + "px";
    }
    requestAnimationFrame(update);
}
//lancement du rafraichissement
requestAnimationFrame(update);
```

Voici le même programme pour plusieurs éléments :

**NB : Cliquer dans le cadre pour activer/arrêter l'animation!**

```javascript
render-d//pour éviter de faire s'exécuter le script en continu (et surcharger la machine)
//on ajoute un système de on/off au click de souris
var canUpdate = false;

window.addEventListener("click", setUpdate);

function setUpdate() {
    if(canUpdate === true) {
        canUpdate = false;
    } else {
        canUpdate = true;
    }
}
//nombre total d'éléments désirés
var totalElement = 10;
//coordonnées x
var x = [];
//vitesse en x, qui bascule entre positif et négatif
var speedX = [];
//acces simplifié à la largeur de l'élément
var width = [];
//tableau pour nos div
var allDiv = [];

//construction de la scène
for(var i=0; i<totalElement; i++ ) {

    x[i] = i*5; // ici, on aura donc 0*5, puis 1*5, 2*5, etc.
    speedX[i] = i + 1; //ici, on aura 0+1, 1+1, 2+1, etc.
    width[i] = 100;

    var div = document.createElement("div");
    div.style.position = "absolute";
    div.style.width = width[i] + "px"; //on utilise notre tableau ici
    div.style.height = "100px";
    div.style.border = "solid 1px black";
    document.body.appendChild(div);

    allDiv[i] = div; //stockage des div
}

//fonction utilisée pour le rafraichissement du programme
function update() {
    
    if(canUpdate) {

        for(var i=0; i<totalElement; i++ ) {

            if(x[i] > window.innerWidth - width[i]) {

                speedX[i] = -speedX[i]; // on inverse la direction ici
            }

            if(x[i] < 0) {
                speedX[i] = -speedX[i]; // on inverse la direction ici aussi
            }
            //calcul de la nouvelle coordonnée
            x[i] += speedX[i];

            //application de la coordonnée
            allDiv[i].style.left = x[i] + "px";
        }
    }
    requestAnimationFrame(update);
}
//lancement du rafraichissement
requestAnimationFrame(update);
```

Bien comprendre l'usage des boucles est donc indispensable, tout autant que l'usage des tableaux. La maitrise de ces deux éléments de programmation n'est pas aisée, elle demande beaucoup de pratique, d'entraînement. Mais une fois que ces principes sont acquis, vos possibilités seront très largement augmentées.

Dans l'exemple ci-dessus, on peut voir que la variable "compteur" ```i```, déclarée dans la boucle ```for```, est utilisée de plusieurs manières :
- comme index permettant d'accéder à une cellule des tableaux (```x[i]```, ```speedX[i]```, etc.).
- comme valeur permettant de calculer une coordonnée particulière par cellule du tableau (```x[i] = i * 5```).


## Code source complet

**NB : Cliquer dans le cadre ci-dessous pour activer le rafraichissement graphique.**

```javascript
render-e//pour éviter de faire s'exécuter le script en continu (et surcharger la machine)
//on ajoute un système de on/off au click de souris
var canUpdate = false;

window.addEventListener("click", setUpdate);

function setUpdate() {
    if(canUpdate === true) {
        canUpdate = false;
    } else {
        canUpdate = true;
    }
}

//construction de la scène
var elementTotal = 50;

var divs = [];

var x = [];
var y = [];

var speedX = [];
var speedY = [];

var minSpeedX = 1;
var maxSpeedX = 5;
var minSpeedY = 1;
var maxSpeedY = 5;

var width = [];
var height = [];

var minWidth = 5;
var maxWidth = 20;
var minHeight = 5;
var maxHeight = 20;

for (var index = 0; index < elementTotal; index++) {

    width[index] = randomFromTo(minWidth, maxWidth);
    height[index] = randomFromTo(minHeight, maxHeight);

    x[index] = randomFromTo(0, window.innerWidth - width[index]);
    y[index] = randomFromTo(0, window.innerHeight - height[index]);

    speedX[index] = randomFromTo(minSpeedX, maxSpeedX);
    speedY[index] = randomFromTo(minSpeedY, maxSpeedY);

    var div = document.createElement("div");
    div.style.position = "absolute";
    div.style.left = x[index] + "px";
    div.style.top = y[index] + "px";
    div.style.width = width[index] + "px";
    div.style.height = height[index] + "px";
    div.style.border = "solid 1px black";
    document.body.appendChild(div);

    //conserve une référence à chaque div dans un array
    divs[index] = div;
}

function update() {

    if (canUpdate) {

        for (var index = 0; index < elementTotal; index++) {

            //suite de conditions pour gérer la vitesse et donc le rebond
            if (x[index] > window.innerWidth - width[index]) {
                speedX[index] = -speedX[index];
                x[index] = window.innerWidth - width[index]; //on contraint absolument la coordonnée à la limite de la fenêtre
            }
            if (x[index] < 0) {
                speedX[index] = -speedX[index];
                x[index] = 0; //on contraint absolument la coordonnée à la limite de la fenêtre
            }

            if (y[index] > window.innerHeight - height[index]) {
                speedY[index] = -speedY[index];
                y[index] = window.innerHeight - height[index]; //on contraint absolument la coordonnée à la limite de la fenêtre
            }
            if (y[index] < 0) {
                speedY[index] = -speedY[index];
                y[index] = 0; //on contraint absolument la coordonnée à la limite de la fenêtre
            }

            //on applique la vitesse (toujours après les conditions)
            x[index] += speedX[index];
            y[index] += speedY[index];

            //on applique à l'élément graphique (toujours après les calculs)
            divs[index].style.left = x[index] + "px";
            divs[index].style.top = y[index] + "px";
        }
    }
    requestAnimationFrame(update);
}

//répétition au fil du temps
requestAnimationFrame(update);

//fonction utilitaire : tirage au hasard d'un nombre entre deux valeurs
function randomFromTo(from, to) {
    return Math.random() * (to - from) + from;
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}