[Retour à l'index](index.html)

# Mémo JavaScript

## Structure de controle : les conditions avec _if_

Un programme informatique est constitué d'une suite d'instructions qui permettent de décrire des traitements de données divers. Pour écrire et décrire ces traitements, il est important de respecter un certain ordre. Pour séquencer les traitements, il faut pouvoir contrôler leur flux. C'est ce à quoi servent les structures de contrôle : à organiser le séquençage de l'exécution de notre programme.

L'idée de base des conditions est très simple :

```
SI la condition est remplie
ALORS il se passe une chose
OU une autre
```

Il s'agit donc d'une structure logique.

## Syntaxe

Une condition `if`, se construit ainsi :

```javascript
if( valeur_à_tester opérateur_de_comparaison valeur_de_comparaison) {
  //bloc d'instructions à exécuter
}
```

Quelques exemples simplistes :

```javascript
if (1 > 2) {
  //résultat du test = false
  console.log("condition remplie"); //cette ligne de code ne sera pas exécutée
}

if (1 < 2) {
  //résultat du test = true
  console.log("condition remplie"); //cette ligne de code sera bien exécutée
}
```

On voit donc que la condition est décrite dans les ```()```. Il s'agit en réalité d'une opération qui utilise l'un des opérateurs de comparaison disponible dans le langage que l'on utilise.
En JavaScipt, ces opérateurs sont listés ici : [Opérateurs de comparaison](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/Op%C3%A9rateurs_de_comparaison){target=blank}

Les plus fréquents sont :
* l'égalité ```==```
* l'inégalité ```!=``` (aussi appelé "not", de l'anglais "n'est pas")
* supérieur ```<```
* inférieur ```>```
* supérieur ou égal ```<=```
* inférieur ou égal ```>=```

## SI - ALORS - OU

Le mot clef ```else``` permet de définir ce qui doit être exécuté si la condition n'est pas remplie :

```javascript
if (1 < 2) { //SI
  //résultat du test = true, ALORS
  console.log("condition remplie");
} else { //OU
    //résultat du test = false, ALORS
    console.log("condition non remplie");
}
```

Une variante existe pour préciser un condition complémentaire lorsque la condition d'origine est fausse : ```else if```.
Ci-dessous, un exemple complet d'usage de ```if(){ }else if(){ }```.

```javascript
render-a//variables globales, accessibles partout dans le programme
var maxSteps = 10;
var currentStep = 0;

//fonction appellée à chaque intervalle de temps
function update(){

    if (currentStep < maxSteps) { //SI
        //ALORS
        //... un traitement à faire autant de fois que maxSteps le détermine
        currentStep += 1;//incrémente l'étape actuelle
        document.body.innerHTML = currentStep;//un rendu dans la page
    } else if (currentStep >= maxSteps){ //OU SI
        //ALORS
        currentStep = 0;//arrêt du processus
        clearInterval(timer);//annulation du timer, la répétition n'aura plus lieu
    }
}

//mise en place d'un processus qui se répète, défini ici dans la fonction update, toute les 1000 ms (1s)
var timer = setInterval(update, 1000); //la fonction de callback doit être définie avant!
```
```css
render-abody {
    font-family: Menlo, monospace;
    font-size: 150px;
    margin-left: 40%;
}
```

Ici, on définit un nombre d'étapes maximum à faire passer et l'étape courante. On construit donc une forme de compteur, qui permet de faire s'exécuter des instructions un certain nombre de fois puis de stopper le processus.
Dans ce genre de situation, l'utilisation de ```if(){ }else if(){ }``` facilite l'écriture.

## Association et accumulation des conditions

Pour associer plusieurs conditions entre elles, il existe plusieurs méthodes, dont l'usage dépendra du design du projet.

```
SI ceci ET cela
ALORS

SI ceci OU cela
ALORS
```

```javascript
//l'opérateur && (and) permet d'associer plusieurs conditions qui doivent toutes être remplies
if(condition_1 && condition_2) {
    //on arrive jusqu'ici uniquement si les deux conditions sont évaluées à "true"
}

//l'opérateur || (or) permet de cumuler plusieurs conditions dont au moins l'une d'entre elles doit être remplie
if(condition_1 || condition_2) {
    //on arrive jusqu'ici uniquement si l'une des deux conditions est évaluée à "true"
}
```

On peut aussi imbriquer les conditions les unes dans les autres, pour aboutir à des structures conditionnelles complexes :

```javascript
if(condition_1) {
    //code à exécuter, ou rien
    if(condition_2) {
         //code à exécuter, ou rien
        if(condition_3) {
            //code à exécuter
        }
    }
}
```
(*On voit ici que l'indentation du code source devient indispensable pour comprendre ce qu'on écrit. L'usage d'un éditeur de code rend l'écriture beaucoup plus confortable!*)

## Pour aller plus loin

Observons une structure comme celle-ci :

```javascript
var a = 1;
var b = 2;
var c = 3;

var test = (a < b && b < c) || (c > b && b > a );

console.log("test = ", test);
```
> test = true

Le test est ici purement démonstratif, les valeurs des trois variables font que le résultat sera vrai. Il s'agit ici de voir que, d'une part, les tests peuvent être effectués dans n'importe quel contexte, et pas uniquement à l'intérieur des structures ```if()```. Mais on voit aussi que cette écriture rappelle beaucoup la notation de système logique. [L'algèbre de Boole](https://fr.wikipedia.org/wiki/Alg%C3%A8bre_de_Boole_(logique)){target=blank} en particulier a eu une grande importance dans la naissance de l'informatique et son influence sur les langage de programmation est indéniable.

### inline if

Une écriture très compacte d'un ```if``` peut tenir en une seule ligne. On parle alors d'un ```inline if```, puisque la structure de contrôle est tient sur une seule et même instruction. Cette écriture peut être utile pour réduire le nombre de ligne de code d'un programme qui utilise beaucoup de ```if``` suffisament simples pour être exprimés sur une seule ligne.

```javascript
var a = 1;
var b = 2;
var result = false;

//structure habituelle avec if
if( a < b ){
    result = true;
}
else {
    result = false;
}

//structure inline
result = (a < b) ? true : false
```

Dans l'exemple ci-dessus, la version ```inline``` est construite ainsi :

```javascript
/*
(expression_à_tester) ? (alors) expression : (sinon) expression

SI a < b ALORS result = true SINON result = false

converti en inline if :
*/
result = (a < b) ? true : false
```

On voit que ```?``` permet d'executer une expression si le test résulte à ```true```, et que ```:``` correspond à ```else {}```, autrement dit à l'expression qui doit être exécutée si le test produit la réponse ```false```.


[Retour à l'index](index.html)

Fin.{style="opacity:0"}
