[Retour à l'index](index.html)
# Mémo JavaScript

## Image-Map : image à zones sensibles

Une image est présentée dans la page. Elle peut être en pleine page, ou en occuper qu'une partie. Certains éléments visuels présents dans cette image permettent de faire apparaître des informations de diverses nature : des paragraphes de texte, des images, des vidéos, etc. L'interaction est simple : en passant la souris par-dessus les éléments de l'image, ces informations complémentaires apparaissent; elles disparaissent lorsque la souris sort de l'élément de l'image. On peut parle de "zones sensibles" pour ces parcelles interactives de l'image de fond. Cette image étant comme découpée en un certain nombre de zones "géographiques", elle devient une forme de carte, d'où le nom **image-map** pour ce projet.

Ce type de projet est particulièrement classique et fréquent. Malgré sa grande simplicité, il présente une certaine efficacité. Aussi, son principe peut être adapté à un très grand nombre de situations. Par exemple, on peut imaginer qu'une vidéo se met en pause à un temps précis et qu'une séquence interactive fondée sur ce principe de zones sensibles permet d'accéder à des informations complémentaires (des vidéos annexes par exemple), avant que la vidéo ne se remette en lecture.

On pourrait ici parler d'un "modèle interactif", dans le sens ou un certain design des interactions est déjà prévu, avec ses outils techniques, mais les médias utilisés peuvent varier à volonté. Ainsi, si le design d'interaction est le même, le propos véhiculé par le projet peut varier du tout au tout selon les choix esthétiques qui sont faits.

Trouvons les solutions techniques qui permettent à un tel modèle d'exister!

## Les blocs techniques

- L'image de fond
  - Est-elle en pleine page ?
    - Si oui, le système doit être "*responsive*" (il s'adapte à la taille de la page et de l'écran en temps réel)
    - Sinon, il faut rester à une échelle fixe, le redimensionnement de la fenêtre ne faisant que masquer des parties de la scène.

- Les zones sensibles
  - Comment les placer dans l'image ?
    - Elles doivent être dans le même référentiel géométrique que l'image : leurs coordonnées doivent être définies à partir de la position de l'image dans la page
    - Elles doivent être placées *par-dessus* l'image, comme un calque posé en avant-plan.
    - Nous connaissons les interactions souris simples, gérées par des événements (```"click"```, ```"mouseenter"```, etc.). Nous devrions pouvoir les utiliser ici.

- Les informations annexes
  - Elles doivent pouvoir être positionnées librement dans l'espace de la page (sans être nécessairement attachées à l'image).
  - Leur type doit être quelconque et simple à définir.

## Les techniques envisagées

D'après la liste des blocs techniques dressée ci-dessus, nous pouvons faire une hypothèse de structure technique pour ce projet.

L'image de fond, qui doit pouvoir être changée aisément, pourrait être définie dans le fichier HTML. Il en va de même pour les zones sensibles et les informations annexes : les insérer dans une arborescence HTML facilite la compréhension de l'organisation des différents éléments de la scène.

CSS nous permettrait de définir le type de positionnement et les coordonnées de chaque élément. Aussi, l'usage de classes permet d'affecter un même ensemble de règles plusieurs éléments.

JavaScript nous aidera à gérer tout ce que les autres langages ne peuvent pas faire, l'interactivité en premier lieu.

## Chaque chose en son temps...

### L'image de fond

Pour l'exercice, nous utiliserons l'image d'une grille régulière. Cela facilitera notre compréhension des traitements que l'image pourrait subir par la feuille de style que nous allons mettre en place.

**NB** : *Sous Safari et Chrome les iFrames sont redimensionnables verticalement! Le coin inférieur droit vous permet donc d'agrandir les cadres de rendu!*

```html
render-a<html>

    <head>
        <meta charset="utf8">
    </head>

    <body>
        <img src="rsrc/img/grid-small.png">
    </body>

</html>
```

Plusieurs problèmes apparaissent ici : une marge est appliquée par défaut à l'ensemble du document, et l'image est "coincée" dans le coin supérieur gauche de la page. Une feuille de style va nous aider à résoudre cette situation.

Nous allons :
- ajouter un ```id``` à l'image pour l'atteindre via css.
- ajouter un fichier css avec les règles suivantes :
  - mise à 0 de la marge sur l'ensemble du document.

```html
render-brender-c<html>

    <head>
        <meta charset="utf8">
        <link type="text/css" href="style.css" rel="stylesheet">
    </head>

    <body>
        <img src="rsrc/img/grid-small.png" id="map">
    </body>

</html>
```
```css
render-bhtml,
body {
  margin: 0px;
  width: 100%;
  height: 100%;
}
```


On progresse, mais ce n'est pas encore idéal... L'image de fond, même si elle n'est pas pleine page, pourrait au moins être décalée à l'aide d'une marge...

Nous n'allons pas essayer tout de suite de centrer l'image dans la page, car cela fait apparaître de très nombreux problèmes! Nous réglerons ce centrement dans un prochain épisode.

Pour l'instant, **ajoutons une marge fixe**.

```css
render-chtml,
body {
  margin: 0px;
  width: 100%;
  height: 100%;
}

#map {
  position: absolute;
  margin-left: 100px;
  margin-top: 50px;
}
```

### Les zones sensibles

Maintenant, il faudrait pouvoir placer des zones dans cette image, mais notre structure HTML n'est peut-être pas idéale pour le faire : l'image est placée directement à l'intérieur du ```body```, son point de référence est donc à ce niveau. Il faudrait pouvoir ajouter un élément *enfant* à l'image, pour partager son point de référence et pouvoir placer facilement nos zones sensibles dedans.

Mais une image ne peut pas contenir de balise enfant. En effet, ```<img>``` est "autofermant", on ne peut pas lui associer une balise de fermeture ```</img>```! Il faut donc passer par une autre astuce : l'usage d'un **conteneur**, une div dont la seule mission est d'accueillir l'image, à 100% de ses dimensions, **et** les zones sensibles. Ainsi, le point de référence de l'image et des zones sensibles pourra être partagé :

```html
render-d<html>

    <head>
        <meta charset="utf8">
        <link type="text/css" href="style.css" rel="stylesheet">
    </head>

    <body>
        <div id="container">
            <img src="rsrc/img/grid-small.png" id="map">
            <div id="zone1"></div>
        </div>
    </body>

</html>
```

```css
render-dhtml,
body {
  margin: 0px;
  width: 100%;
  height: 100%;
}

#container {
  border: solid black 1px;
  position: absolute;
  margin-left: 100px;
  margin-top: 50px;
}

#zone1 {
  position: absolute;
  background-color : red;
  left: 20px;
  top: 20px;
  width: 20px;
  height: 20px;
}
```

On voit que la ```zone1``` est ajoutée au même niveau que l'image, à l'intérieur du conteneur. La feuille de style permet donc de positionner cette zone à l'intérieur du conteneur, sur l'image.

Avez-vous remarqué la gestion des règles de ```position``` ? Et aussi, nous n'utilisons plus de régle pour l'image (```#map```), car ce n'est plus utile.

### Interactivité

Passons au comportement de notre zone sensible : au survol sur celle-ci, un autre élément apparaît dans la page.

Nous savons déjà faire quelque chose comme ça. Il nous suffit d'ajouter l'élément dans la structure HTML et de le styliser via CSS.

Notre algorithme est assez simple :
```
Si la souris entre dans la zone
  alors
    l'élément d'information attaché apparaît

Si la souris sort de la zone
  alors
    l'élément d'information attaché disparaît
```

Il faut maintenant traduire ça dans notre environnement de travail. Voici ce que ça pourrait donner :

```javascript
//références aux éléments HTML
var zone1 = document.getElementById("zone1");
var infoZone1 = document.getElementById("zone1Info");

//ajout des écouteurs d'événements
zone1.addEventListener("mouseenter", zone1EnterHandler);
zone1.addEventListener("mouseleave", zone1LeaveHandler);

//gestionnaire d'événements
function zone1EnterHandler() {
  infoZone1.style.display = "block";
}

function zone1LeaveHandler() {
  infoZone1.style.display = "none";
}

```

Notre projet est pratiquement terminé. En voici une version finalisée. On y voit que chaque zone sensible et l'information qui lui correspond sont traitées "manuellement" : l'ajout est fait à la fois dans les fichiers HTML, CSS comme JS.

**Cette manière de procéder, quelque peu artisanale, pourrait être simplifiée à bien des égards**. Mais, ici aussi, chaque chose en son temps...

## Code source complet

```html
render-z<html>

    <head>
        <meta charset="utf8">
        <link type="text/css" href="style.css" rel="stylesheet">
    </head>

    <body>
        <div id="container">
            <img src="rsrc/img/grid-small.png" id="map">
            <div id="zone1" class="zone"></div>
            <div id="zone2" class="zone"></div>
        </div>

        <div id="infoZone1">
            Ce sont des informations sur la zone 1
        </div>

        <div id="infoZone2">
            Ce sont des informations sur la zone 2
        </div>

        <script src="script.js"></script>
    </body>

</html>
```

```css
render-zhtml,
body {
  margin: 0px;
  width: 100%;
  height: 100%;
}

#container {
  border: solid black 1px;
  position: absolute;
  margin-left: 100px;
  margin-top: 50px;
}

#zone1 {
  position: absolute;
  left: 20px;
  top: 20px;
  width: 80px;
  height: 80px;
  background-color: blue;
}

#zone2 {
  position: absolute;
  left: 200px;
  top: 360px;
  width: 65px;
  height: 85px;
  background-color: green;
}

#infoZone1 {
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100px;
  height: 200px;
  background-color: red;
  display: none;
}

#infoZone2 {
  position: absolute;
  left: 620px;
  top: 320px;
  width: 100px;
  height: 200px;
  background-color: brown;
  display: none;
}
```

```javascript
render-z//références aux éléments HTML
//zone 1
var zone1 = document.getElementById("zone1");
var infoZone1 = document.getElementById("infoZone1");

//ajout des écouteurs d'événements
zone1.addEventListener("mouseenter", zone1EnterHandler);
zone1.addEventListener("mouseleave", zone1LeaveHandler);

//gestionnaire d'événements
function zone1EnterHandler() {
  infoZone1.style.display = "block";
}

function zone1LeaveHandler() {
  infoZone1.style.display = "none";
}

//zone 2
var zone2 = document.getElementById("zone2");
var infoZone2 = document.getElementById("infoZone2");

//ajout des écouteurs d'événements
zone2.addEventListener("mouseenter", zone2EnterHandler);
zone2.addEventListener("mouseleave", zone2LeaveHandler);

//gestionnaire d'événements
function zone2EnterHandler() {
  infoZone2.style.display = "block";
}

function zone2LeaveHandler() {
  infoZone2.style.display = "none";
}

```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}