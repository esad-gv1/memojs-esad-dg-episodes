[Retour à l'index](index.html)
# Mémo JavaScript

## Événement clavier : faire apparaître des éléments

Petit exercice permettant d'introduire les événements clavier : en appuyant sur la barre espace, un nouvel élément HTML apparaît dans la fenêtre.

Les interactions clavier sont utiles dans de nombreux cas et peuvent déclencher divers type d'actions.

### Comment s'y prendre ?

Découpons d'abord le problème en plusieurs sous-problèmes.

1. Qu'est-ce que la barre espace ?

    - C'est l'une des touches du clavier
    - Une touche de clavier renvoie une certaine valeur lorsqu'elle est enfoncée : un code numérique. Ce sera notre information principale pour travailler. Les codes associés aux touches sont organisés dans une table de correspondance : à chaque caractère correspond un nombre qui le représente et permet, ensuite, d'y associer un glyphe (un caractère dessiné). L'ASCII est l'un des premiers standards instaurant ces correspondances ([Article Wikipédia](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target=blank}). On utilise majoritairement le standard Unicode, table qui contient la plus grande partie des caractères existants dans de très nombreuses langues ([Définition du standard Unicode](http://www.unicode.org/standard/principles.html){target=blank}).
    - Déclencher une action après la pression sur la barre espace, c'est, autrement dit, obtenir le résultat produit par une interaction sur la barre espace.
    - Après sa capture, il faut mettre œuvre l'interaction : déclencher l'apparition des images.

2. Comment faire apparaître de nouveaux éléments dans la fenêtre

    - Éléments techniques
        - Créer une nouvelle balise HTML (c'est à dire des éléments HTML) à chaque pression sur la touche.
        - Cette balise peut être de n'importe quel type : ```<img> <div> <canvas> <video>``` etc.
        - On peut appliquer un style à chaque nouvel élément pour spécifier son affichage (et sa mise en page).
        
    - Éléments de design
        - S'il s'agit d'images, que représentent-elles ?
        - Quelles sont leurs dimensions ?
        - Quelles sont leurs positions dans la fenêtre ?
        - Apparaissent-elles les unes après les autres ? Ou par petits groupes ?
        
Les spécifications de ce projet sont assez incomplètes, car elles ne précisent pas quels sont les contenus utilisés et surtout ce pour quoi ils sont utilisés. En d'autres termes, le **propos du projet n'est pas donné, sa dimension esthétique est inconnue**. Il faudrait, bien sûr, qu'elle le soit, sans quoi le projet se résume à un simple exercice technique.

Mais nos petites démonstrations ne prétendent pas à faire œuvre : à vous de mettre dans sens dans les choix des éléments affichés, leur mode d'apparition, le tout en lien avec l'interaction utilisée. C'est cet ensemble technico-esthétique que l'on nommera du **design d'interaction**, ou encore du **design de l'interactivité**.

### Principes techniques en JavaScript

En faisant un effort de traduction de ce que nous avons observé plus haut, les briques techniques dont nous avons besoin sont les suivantes :

- Créer des balises ```<div>``` (pour l'instant) à chaque pression sur la touche :

```javascript
var div = document.body.createElement("div");
```

- Si cette balise est une image ```<img>``` ira chercher la srouce image dans un dossier :

```javascript
var img =  document.body.createElement("img");
img.src = "url/to/image/file.extension"; //attention, événement asynchrone! L'image apparaîtra avec un délai
```
            
- Appliquer un style à chaque nouvelle image :

```javascript
div.style.position = "absolute";
div.style.left = "10px";
div.style...
```
        
- Pour produire l'interactivité, le programme doit :

  - Capturer la pression sur barre espace, via un événement de type "keypress".
  
    Remarque : ici on place un écouteur d'évément sur l'objet Window de façon à capturer des événements clavier le plus largement possible (la fenêtre du navigateur doit) :

```javascript
window.addEventListener("keypress", keypressHandler);

function keypressHandler(event)
{
    //vérifier le code de la touche actuellement enfoncée
    console.log(event.keyCode);
    //dans la console, on peut voir 97 si on appuie sur A, 32 si on appuie sur 
}
```
NB : d'autres usages de l'événement de clavier sont possibles : [référence de Keyboard Event](https://developer.mozilla.org/fr/docs/Web/API/KeyboardEvent){target=blank}

- Limiter l'interaction à la seule barre espace.

    La table ASCII nous donne les correspondances, qu'il est donc simple de vérifier (Space = 32), mais un test à l'aide du gestionnaire d'événement ci-dessus permet aussi d'obtenir la valeur souhaitée.
    
    À partir de ce nombre il faut permettre l'exécution d'une suite d'instructions seulement si le nombre correspondant à la barre espace est retourné :
    
    ```
    SI le keyCode est égal à 32 ALORS
        ajouter un élément HTML à la fenêtre
    ```
    
    ```javascript
    if( event.keyCode == 32 ){ 
        ...
    }
    ```
    NB : ```==``` est l'opérateur de comparaison, souvent utilisé dans les structures de contrôle conditionnelles comme
    
    ```if(){ }```. A ne pas confondre avec l'opérateur d'affectation ```=```.
    
## Code source complet

```javascript
render-a//pour éxécuter ce programme correctement ci-dessous, pensez bien à cliquer d'abord dans le cadre (iFrame)
//pour que le clavier lui soit associé
window.addEventListener("keypress", keypressHandler);

function keypressHandler(event)
{
    console.log("keyCode actuel = ", event.keyCode);
    
    if( event.keyCode == 32 )
    {
        console.log("espace!");
        
        //construction de la div
        var div = document.createElement("div");
        div.style.width = "100px";
        div.style.height = "100px";
        div.style.border = "solid black 1px";
        
        //ajout au body de la nouvelle div
        document.body.appendChild(div);
    }
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}