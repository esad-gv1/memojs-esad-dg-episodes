[Retour à l'index](index.html)
# Mémo JavaScript

## De l'aléatoire ?

Il est fréquent d'avoir besoin de tirer des valeurs au hasard, pour laisser le programme choisir à notre place (d'une certaine façon). Cela peut concerner aussi bien des positions dans la page que des couleurs, ou les dimensions d'éléments visuels.
Le principe reste toujours le même : on utilise une fonction particulière qui permet de définir une valeur numérique au hasard. C'est le célèbre ```random``` des programmes informatiques. Notons tout de suite qu'il s'agit, dans les faits, d'une fonction qui génére une très, très longue suite de nombres qui n'ont a priori aucune relation logique dans leur enchaînement. Les nombres pris un à un dans cette liste donnent la sensation d'un tirage au sort hasardeux, mais en réalité, cette longue liste finie par se répéter. C'est pourquoi on parle de générateurs de nombres pseudo-aléatoire.

## Math.random()

En JavaScript, la fonction ```random``` fait partie d'une suite de **méthodes** de l'**objet** ```Math``` : [Math.random()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/random){target=blank}.

```javascript
//dans une console, on écrit à plusieurs reprises :
Math.random();
// > 0.219238316335588
// > 0.7535342201960107
// > 0.24672337243940468
```
***
**NB** : On écrit bien ```Math.random()``` et non ```Math.random```.

**Avec** ```()``` on demande l'**exécution** de la méthode ```random``` de l'objet ```Math``` : on obtiendra donc un résultat.  
**Sans** ```()``` on demande une **référence** à la méthode ```random``` de l'objet ```Math``` : on obtiendra une description de la méthode (ou de la fonction), c'est-à-dire son code source, lorsqu'il n'est pas compilé. C'est aussi ce que l'on passe comme argument dans le système à ```callback```, comme les gestionnaires d'évenements.

Faites le test en écrivant les deux dans une console, vous verrez le résultat!
***
```Math.random();``` retourne donc un nombre entre 0 et 1. Ce qui est bien, mais pas toujours très adapté. Il faut donc pouvoir **mettre de nombre à l'échelle**. La démarche est assez simple : on multiplie le nombre retourné par ```Math.random()``` et on le multiplie par la valeur maximum que l'on souhaite. Ainsi, pour un nombre entre 0 et 255, on fera ```Math.random() * 255```.

Un exemple simple est lorsque l'on souhaite positionner un élément aléatoirement à l'intérieur de la page, entre l'extrémité gauche et l'extrémité droite.  
On sait que la limite gauche de la page correspond à une coordonnée x de 0, et la limite gauche nous est donnée par ```window.innerWidth``` :

```javascript
//Tirage au sort d'une coordonnée x entre les limites horizontales (gauche et droite) de la fenêtre
var randomX = Math.random() * window.innerWidth;
```

Pour obtenir un nombre aléatoire entre deux valeurs, une de départ et une d'arrivée (ou bien un minimum et un maximum), il y a un peu de calcul à faire. La fonction ci-dessous peut être utilisée dans différents projets :

```javascript
//la fonction randomFromTo prend 2 arguments qui seront utilisés pour effectuer le calcul de tirage aléatoire entre un mininum (from) et un maximum (to).
function getRandomFromTo(from, to) {
    var result = Math.random() * (to - from) + from;
    return result; //on retourne la valeur de la variable result
}
```
Exemples de résultats :

```javascript
//dans une console, on écrit à plusieurs reprises :
getRandomFromTo(100, 1000);
// > 157.2257738353294
// > 551.5784931165265
// > 808.3604524516273
```

On peut aussi définir d'autres **fonctions utilitaires** du même type, ici pour construire une couleur CSS, c'est à dire l'assemblage de 3 valeurs entières (r, g, b) tirée au sort entre 0 et 255 pour former une chaîne de caractères comme ```"rgb(0,0,0)"```.

```javascript
//Récupère une couleur CSS aléatoire sur les 3 canaux de couleur rgb
function getRandomRGBColor() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
}
```


[Retour à l'index](index.html)

Fin.{style="opacity:0"}