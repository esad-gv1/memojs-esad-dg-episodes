[Retour à l'index](index.html)

# Mémo JavaScript

## Transformer un bloc de texte en un ensemble d'éléments séparés

Pour que chaque mot composant un texte puisse être rendu interactif, il est nécessaire de pouvoir agir indépendamment sur chacun des mots. Mais les éléments HTML peuvent contenir des blocs de texte long que des règles de style viennent mettre forme.
On pourrait donc avoir besoin d'agir indépendamment sur certains éléments d'un bloc de texte tout en gardant sa mise en forme initialement fournie par une feuille CSS.

Un projet simple démontrant ce besoin serait de faire disparaitre progressivement les mots d'un texte lorsque la souris passe sur ces derniers. Il ne faut donc pas que cette disparition s'applique sur l'ensemble d'un bloc de texte, comme lorsqu'il s'agit d'un élément `<div>` ou bien `<p>`. Elle doit bien d'appliquer sur chacun des mots.

### Comment s'y prendre ?

L'idée est de trouver une méthode pour convertir un bloc de texte en une suite d'éléments indépendants auxquels on pourra ensuite facilement ajouter un écouteur d'événements.

**L'élément `<span>` est bien adapté à ce problème.**

[Référence de l'élément span](https://developer.mozilla.org/fr/docs/Web/HTML/Element/span)

Mais il faut trouver un moyen pour passer d'un bloc de texte contenu dans un élément HTML vers une suite d'élément `<span>`.

### String.split()

Via JavaScript, on peut utiliser une chaîne de caractères quelconque, quelle soit déclarée dans le programme ou qu'elle provienne d'un élément HTML déjà ajouté dans le document, et la découper en une liste d'éléments.

```javascript
//déclaration d'un chaîne de caractère (string)
var monTexte = "Cette chaîne de caractères sera à couper en petits morceaux!",

//récupération du contenu d'un élément HTML existant sous la forme d'un string
var monTexte = document.getElementById("idDeMonElement").innerText;

//ou bien pour avoir le contenu formaté en HTML
var monTexte = document.getElementById("idDeMonElement").innerHTML;
```

**Le découpage peut être réalisé avec l'une des méthodes de l'objet String : split.**

[Référence de l'objet String, méthode split](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/split)

Cette méthode renvoie **un tableau (Array)** de chaînes de caractères, c'est-à-dire une liste composée de tous les éléments résultants du découpage de la chaîne.

### Array et accès par index

[Référence de l'objet Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array)

```javascript
//On découpe la chaîne de caractères à l'aide du séparateur " " (espace)
var listeDeMots = monTexte.split(" ");
console.log(listeDeMots);

//résultat dans la console :
//["Cette", "chaîne", "de", "caractères", "sera", "à", "couper", "en", "petits", "morceaux!"] (10)
```

Il faut donc pouvoir parcourir cette liste pour utiliser son contenu et produire une suite d'éléments `<span>`. Mais une liste, un Array, ne s'utilise par comme une variable simple. Il faut pouvoir accéder à chacun de ses éléments pour en obtenir la valeur. On utilise donc un index.

Ici, `listeDeMots[0]` donnera la valeur `"Cette"` et `listeDeMots[3]` donnera `"caractères"`. **Les crochets qui suivent le nom de la variable représentant l'Array contiennent donc l'index de l'élément auquel on veut accéder.**
Pour connaître le nombre total d'élement dans un Array on utilise la propriété `length` :

```javascript
console.log(listeDeMots.length);

//résultat dans la console :
//10
```

NB: pour accéder au dernier élément d'un Array, on peut utiliser `monArray[monArray.length-1]`, car le nombre qui est entre crochets peut être calculé, il peut donc aussi provenir d'une variable déclarée plus haut. L'important est qu'il s'agisse d'un nombre entier (pas de virgule!) et qu'il ne soit pas plus haut que la taille de l'Array. monArray[10] renvoie une valeur indéfinie (`undefined`), car la taille de l'Array est de 10 éléments. Le premier élément étant à l'index 0, le dernier est 9 (de 0 à 9, on a bien 10 index).

### Parcourir l'Array de String : boucle

Pour parcourir notre Array contenant tous nos mots, le plus simple et efficace est d'utiliser une structure de contrôle qui permet de répéter un bloc d'instructions en ne faisant varier que les éléments nécessaires. Ici, nous savons que nous voulons accéder à tous les index de notre Array, les uns à la suite des autres, sans en délaisser aucun : `monArray[0]; monArray[1]; monArray[2]; ...`. On voit que ce qui varie est ici l'index, et que la varie est simple : on ajoute 1 à chaque fois. **On parle d'une incrémentation.**

Une boucle `for` va nous permettre de parcourir la liste en utilisant une variable qui sera incrémentée à chaque itération (exécution) des instructions qu'on veut répéter.

[Référence des boucles for](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/for)

```javascript
for (var i; i < listeDeMots.length; i++) {
  var unMot = listeDeMots[i];
}
```

Avec tous ces éléments techniques et ceux des précédents épisodes, on peut réaliser notre projet sans difficulté.

## Projet complet

```javascript
render-a/*
Projet : Un texte est affiché dans une page. Lors du passage de la souris sur les mots, leur opacité baisse d'un cran. À force de passage, les mots disparaissent.

Isoler chaque mot du texte = Passer d'un élément composite à un ensemble d'éléments.
Balises contiennent du texte, mais un ensemble de mots -> une balise = un mot = un identifiant (ou un objet).

Idée : Il faut convertir le grand bloc de texte en une suite de balises contenant tous les mots de la fable. -> passer par string.split("séparateur")!! donne un tableau (liste) de chaînes de caractères!
Nombre d'éléments d'un tableau = array.length (idem pour les string)

Interactivité : mouseEvent = mouseover -> opacité | couleur | taille | graisse
Agir sur tous les éléments! -> gestion partagé des événements grâce à this ou event.srcElement

Baisser l'opacité d'un élément à l'écran :
soustraire une valeur à la valeur actuelle de l'opacité
récupérer la valeur actuelle (attention à CSS non accessible!)
faire le calcul !!opacité est entre 0 et 1!!

*/
//référence au texte
var fable =
  "Un laboureur, ayant trouvé un aigle pris au filet, fut si frappé de sa beauté qu’il le délivra et lui donna la liberté. L’aigle ne se montra pas ingrat envers son bienfaiteur ; mais le voyant assis au pied d’un mur qui menaçait ruine, il vola vers lui et enleva dans ses griffes le bandeau qui lui ceignait la tête. L’homme se leva et se mit à sa poursuite. L’aigle laissa tomber le bandeau. Le laboureur le ramassa, et revenant sur ses pas, il trouva le mur écroulé à l’endroit ou il s’était assis, et fut bien étonné d’être ainsi payé de retour.<br><br>Il faut rendre les services qu’on a reçus; car le bien que vous ferez vous sera rendu.";

var fableElement = document.createElement("div");
fableElement.style.fontFamily = "'Menlo', monospace";
document.body.appendChild(fableElement);

//découpage du texte
var fableArray = fable.split(" ");
//création des balises - automatique -> boucle de répétition
//condition de départ = ma variable indexMot est à 0 (représente la position dans la liste)
//condition pour continuer = tant qu'il y a des mots dans la liste
//exécution à chaque itération = incrémentation de l'index (dans la liste)
for (var indexMot = 0; indexMot < fableArray.length; indexMot = indexMot + 1) {
  var newSpan = document.createElement("span");
  newSpan.id = "fableMot" + indexMot;
  newSpan.innerHTML = fableArray[indexMot] + " "; //ajoute un espace après!

  //ajout au texte existant
  fableElement.appendChild(newSpan);

  newSpan.style.opacity = 1;

  //ajout de l'écouteur d'événement
  newSpan.addEventListener("mouseover", function (event) {
    this.style.opacity = this.style.opacity - 0.1;
    console.log("mouseover", this.style.opacity);
  });

  console.log(newSpan);
}
```

[Retour à l'index](index.html)

Fin.{style="opacity:0"}