[Retour à l'index](index.html)

# Mémo JavaScript

## Événements clavier : accumuler les lettres dans une même chaîne de caractère

Une petite variation de l'épisode précédent pour mimer une forme d'éditeur de texte.
Il s'agit d'afficher les caractères des touches du clavier sur lesquelles on appuie dans une div. Ces caractères s'accumulent les uns à la suite des autres pour reproduire ce que fait un traitement de texte basique.

## Concatenation de chaînes de caractères

Pour ajouter des caractères dans une même chaîne, un peut utiliser l'opérateur d'addition `+`.

```javascript
console.log(1 + 1);
// > 2

console.log("1" + "1");
// > 11

var start = "ceci";
var middle = "est";
var end = "cela";

console.log(start + middle + end);
// > ceciestcela

console.log(start + " " + middle + " " + end);
// > ceci est cela
```

Les opérateurs composés peuvent aussi être utilisés, en particulier `+=`, qui effectue une concaténation également :

```javascript
var debut = "C'est le début";
var fin = " de la fin";

var phrase = debut;
console.log(phrase);
// > C'est le début

phrase += fin;
console.log(phrase);
// > C'est le début de la fin
```

## Code source complet

```javascript
render-a//pour exécuter ce programme correctement ci-dessous, pensez bien à cliquer d'abord dans le cadre (iFrame)
//pour que le clavier lui soit associé
window.addEventListener("keypress", keypressHandler);

//construction d'une div pour y insérer le texte
var div = document.createElement("div");
div.style.width = "300px";
div.style.height = "100px";
div.style.border = "solid black 1px";
div.style.fontSize = "30px";
div.style.overflow = "auto"; //pour gérer de dépassement du cadre de la div
div.style.overflowWrap = "break-word"; //organise le retour à la ligne automatiquement

//ajout au body de la nouvelle div
document.body.appendChild(div);

//notre chaîne devant accumuler les caractères provenant du clavier
var text = "";

//la callback pour les événements clavier
function keypressHandler(event) {
  //ajoute le caractère à la chaîne
  text += event.key;
  //remplace le contenu HTML de notre div avec la nouvelle chaîne
  div.innerHTML = text;
}
```

## Remarques

Vous constatez qu'il ne s'agit pas ici d'un traitement de texte, mais bien d'une simulation :
* Il n'y a pas de curseur pouvant être placé à l'endroit de l'édition actuelle,
* Il est impossible d'effacer ce qu'on a écrit, car notre code source ne fait qu'afficher les caractères correspondant aux touches du clavier, il ne leur donne pas de fonction autres (comme effacer, revenir à la ligne, etc.).
* Aucune selection du texte n'est possible (puisqu'il n'y a pas de curseur).

Cela vous laisse un peu imaginer ce qu'est un champ de texte éditable : un objet de design interactif complexe!

## TextArea

Pour utiliser un champ de texte éditable contenant déjà tous les comportements standards absent de notre petite simulation, l'objet [TextArea](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Textarea) peut vous faciliter la tâche. Il permet aussi une manipulation programmée via JavaScript qui peut être très riche et complexe.

[Retour à l'index](index.html)

Fin.{style="opacity:0"}
